#include "Box.h"
#include "Ray.h"
#include "LocalGeo.h"

Box::Box()
{
	mMin = vec3(0.0,0.0,0.0);
	mMax = vec3(1.0,1.0,1.0);
}

bool Box::intersect(const Ray& ray, float& tHit, LocalGeo& local) {
	float tmp;
	float tNear = -INFINITY;
	float tFar = INFINITY;

	vec3 tMin = (mMin - ray.getPosition())/ray.getDirection();;
	vec3 tMax = (mMax - ray.getPosition())/ray.getDirection();;

	if (tMin.x > tMax.x) { 
		tmp = tMin.x; 
		tMin.x = tMax.x; 
		tMax.x = tmp;
	}

	if (tMin.y > tMax.y) { 
		tmp = tMin.y; 
		tMin.y = tMax.y; 
		tMax.y = tmp;
	}

	if (tMin.z > tMax.z) { 
		tmp = tMin.z; 
		tMin.z = tMax.z; 
		tMax.z = tmp; 
	}

	tNear = max(tMin.z, max(tMin.y, max(tMin.x, tNear)));
	tFar = min(tMax.z, min(tMax.y, min(tMax.x, tFar )));

	if (tNear > tFar) 
		return false; // The box is missed.

	if (tFar < 0) 
		return false; // The box is behind us.

	if (tNear < 0) { 
		tHit = tFar; 
	} // We are inside the box.
	else 
		tHit = tNear;

	//Need to take care of the case when it is inside the box
	if (tHit < ray.getTMin() || tHit > ray.getTMax())
		return false;

	vec3 intersection = ray.getPosition() + tHit * ray.getDirection();

	local.setPosition(intersection);
	local.setNormal(getNormal(intersection));

	return true;
}

bool Box::intersectP(const Ray& ray) {
	float tmp;
	float tNear = -INFINITY;
	float tFar = INFINITY;

	float tHit;

	vec3 tMin = (mMin - ray.getPosition())/ray.getDirection();;
	vec3 tMax = (mMax - ray.getPosition())/ray.getDirection();;

	if (tMin.x > tMax.x) { 
		tmp = tMin.x; 
		tMin.x = tMax.x; 
		tMax.x = tmp;
	}

	if (tMin.y > tMax.y) { 
		tmp = tMin.y; 
		tMin.y = tMax.y; 
		tMax.y = tmp;
	}

	if (tMin.z > tMax.z) { 
		tmp = tMin.z; 
		tMin.z = tMax.z; 
		tMax.z = tmp; 
	}

	tNear = max(tMin.z, max(tMin.y, max(tMin.x, tNear)));
	tFar = min(tMax.z, min(tMax.y, min(tMax.x, tFar )));

	if (tNear > tFar) 
		return false; // The box is missed.

	if (tFar < 0) 
		return false; // The box is behind us.

	if (tNear < 0) { 
		tHit = tFar; 
	} // We are inside the box.
	else 
		tHit = tNear;

	//Need to take care of the case when it is inside the box
	if (tHit < ray.getTMin() || tHit > ray.getTMax())
		return false;

	return true;
}

bool Box::intersectP(const AABB& box) {
	return false;
}

void Box::findUV(vec3 position, float& x, float& y) {

}

AABB Box::computeAABB() const {
	return AABB();
}

vec3 Box::getNormal(vec3 & intersection)
{
	vec3 distance = mMin - intersection;

	float minDistance = abs(distance.x);

	int min = 0;

	if (abs(distance.y) < minDistance) { 
		minDistance = abs(distance.y); 
		min=2; 
	}

	if (abs(distance.z) < minDistance) { 
		minDistance = abs(distance.z); 
		min = 4; 
	}

	distance = mMax - intersection;

	if (abs(distance.x) < minDistance) { 
		minDistance=abs(distance.x); 
		min=1; 
	}

	if (abs(distance.y) < minDistance) { 
		minDistance=abs(distance.y); 
		min=3; 
	}

	if (abs(distance.z) < minDistance) { 
		minDistance = abs(distance.z); 
		min = 5; 
	}

	switch (min) {
	case 0: return vec3(-1, 0, 0);
	case 1: return vec3( 1, 0, 0);
	case 2: return vec3( 0,-1, 0);
	case 3: return vec3( 0, 1, 0);
	case 4: return vec3( 0, 0, 1);
	default:  return vec3(0, 0, -1);
	}

	/*switch (min) {
	case 0: return vec3(-1, 0, 0);
	case 1: return vec3( 1, 0, 0);
	case 2: return vec3( 0,-1, 0);
	case 3: return vec3( 0, 1, 0);
	case 4: return vec3( 0, 0,-1);
	default:  return vec3(0, 0, 1);
	}*/
}