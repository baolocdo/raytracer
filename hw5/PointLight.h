#pragma once

#include "stdafx.h"
#include "Light.h"

class PointLight
	: public Light
{
public:
	PointLight(void);
	PointLight(vec3 attenuation);
	~PointLight(void);

	vec3 getPosition() const;
	void setPosition(vec3 position);

	virtual void generateLightRay(LocalGeo& local, Ray& lightRay);
	virtual vec3 shading(LocalGeo& local, Material* material, Ray& lightRay, vec3 eyeDir);

	virtual vec3 lightColor(LocalGeo& local, Material* material, AggregatePrimitive* primitives, Ray& ray);

private:
	vec3 mPosition;
	vec3 mAttenuation;
};

