#include "AreaLight.h"
#include "Ray.h"
#include "LocalGeo.h"
#include "AggregatePrimitive.h"

AreaLight::AreaLight(void)
{
	mPosition = vec3(0.0f);
	mEdge1 = vec3(0.0f);
	mEdge2 = vec3(0.0f);
	mSamplingRate = 20;
	mAttenuation = vec3 (1.0f, 0.0f, 0.0f);
}


AreaLight::~AreaLight(void)
{
}

AreaLight::AreaLight(vec3 position, vec3 edge1, vec3 edge2, vec3 color, vec3 attenuation, int samplingRate) 
	: mPosition(position), mEdge1(edge1), mEdge2(edge2), mAttenuation(attenuation), mSamplingRate(samplingRate)
{
	mColor = color;
}

vec3 AreaLight::shading(LocalGeo& local, Material* material, Ray& lightRay, vec3 eyeDir) {
	float distance = glm::length(mPosition - local.getPosition());
	float attenuation = (mAttenuation.x + mAttenuation.y * distance + mAttenuation.z * distance * distance);
	vec3 returnColor = computeLight(local, material, lightRay, eyeDir);
	return returnColor / attenuation;
}

vec3 AreaLight::lightColor(LocalGeo& local, Material* material, AggregatePrimitive* primitives, Ray& ray) {
	Ray lightRay;
	vec3 color(0.0f);

	for (int i = 0; i < mSamplingRate; i++) {
		generateLightRay(local, lightRay);

		if (glm::dot(local.getNormal(), lightRay.getDirection()) > 0.0f) {
			if (!primitives->intersectP(lightRay)) {
				color += shading(local, material, lightRay, ray.getDirection());
			}
		}
	}

	return vec3(color.x / mSamplingRate, color.y / mSamplingRate, color.z / mSamplingRate);
}

void AreaLight::generateLightRay(LocalGeo& local, Ray& lightRay) {
	vec3 newPointOnLight = generateRandomPoint();

	lightRay.setDirection(glm::normalize(newPointOnLight - local.getPosition()));
	lightRay.setPosition(local.getPosition());

	lightRay.setTMax(glm::length(newPointOnLight - local.getPosition()));
}

vec3 AreaLight::generateRandomPoint() {
	float scaleX = rand() / (float) RAND_MAX;
	float scaleY = rand() / (float) RAND_MAX;

	return scaleX * mEdge1 + scaleY * mEdge2 + mPosition;
}