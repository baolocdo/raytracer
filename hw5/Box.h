#pragma once

#include "stdafx.h"
#include "Shape.h"

#define BOX 4

class Box
	: public Shape
{
public:
	Box();
	Box(const vec3 &min,  const vec3 &max) : mMin(min), mMax(max) {};

	virtual bool intersect(const Ray& rRay, float& tHit, LocalGeo& local);
	virtual bool intersectP(const Ray& ray);
	virtual bool intersectP(const AABB& box);

	virtual void findUV(vec3 position, float& x, float& y);

	virtual AABB computeAABB() const;

private:
	vec3 getNormal( vec3 &intersection );
	vec3 mMin, mMax;
};