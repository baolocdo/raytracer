#pragma once

#include "stdafx.h"

class Scene
{
public:
	Scene(string fileName);
	Scene(string fileName, string outputFile);
	~Scene(void);

	void loadScene(string file);
	void saveToFile();

	virtual void renderToFilm();

	Camera* getCamera() const;
	Sampler* getSampler() const;
	Tracer* getTracer() const;
	Film* getFilm() const;
	vector<Light*> getLights() const;

	int getWidth() const;
	void setWidth(int width);

	int getHeight() const;
	void setHeight(int height);

	int getMaxDepth() const;
	void setMaxDepth(int maxDepth);

	int getSamplingRate() const;
	void setSamplingRate(int samplingRate);

private:
	void cleanUp();
	void reset();
	//vec3 integrateColor(Sample& sample);

	Camera* mCamera;
	Sampler* mSampler;
	Tracer* mTracer;
	Film* mFilm;
	Environment* mEnvironment;

	vector<Primitive*> mPrimitives;
	vector<Primitive*> mLightPrimitives;
	vector<Light*> mLights;

	vector<vec3> mVectices;
	vector<vec3> mNormals;
	vector<vec3> mNormalVertices;

	string mOutputFileName;

	// The maximum depth for a ray. Default 5
	int mMaxDepth;
	int mSamplingRate;
	int mWidth;
	int mHeight;
};