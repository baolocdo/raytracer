#pragma once

#include "stdafx.h"

class Light
{
public:
	Light(void);
	~Light(void);

	virtual void generateLightRay (LocalGeo& local, Ray& lightRay) = 0;

	vec3 getColor() const;
	void setColor(vec3 color);

	vec3 getAmbient() const;
	void setAmbient(vec3 ambient);

	virtual vec3 computeLight(LocalGeo& local, Material* material, Ray& lightRay, vec3 eyeDir);
	virtual vec3 shading(LocalGeo& local, Material* material, Ray& lightRay, vec3 eyeDir) = 0;

	virtual vec3 lightColor(LocalGeo& local, Material* material, AggregatePrimitive* primitives, Ray& ray) = 0;

protected:
	vec3 mColor;
	vec3 mAmbient;
};

