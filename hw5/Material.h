#pragma once

#include "stdafx.h"
#include "BRDF.h"
#include "LocalGeo.h"
#include "Texture.h"

class Material
{
public:
	Material(BRDF brdf);
	~Material(void);

	BRDF getBRDF(const LocalGeo& local) const;

	bool isTextured() const;
	void setIsTextured(bool isTextured);

	Texture* getTexture() const;
	void setTexture(string fileName);

	vec3 getColor(LocalGeo& local);
private:
	BRDF mBRDF;
	bool mIsTextured;
	Texture* mTexture;
};