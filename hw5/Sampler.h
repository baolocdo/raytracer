#pragma once

#include "stdafx.h"
#include "Camera.h"

class Sampler
{
public:
	Sampler(int width, int height);
	virtual vec3 sample(int x0, int y0, Tracer* rayTracer, Camera* camera) = 0;

protected:
	int mWidth;
	int mHeight;
};

