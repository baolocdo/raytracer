#pragma once

#include "stdafx.h"

class KdNode {
public:
	// public methods
	void initLeaf(int* primNums, int primitivesCount);
	void initInterior(int axis, int aboveChildIndex, float splitPosition);

	float getSplitPosition() const;
	int getNumberPrimitives() const;
	int getSplitAxis() const;
	bool isLeaf() const;
	int getSecondChildNodeIndex() const;

public:
	// public members
	union {
		float mSplit; // Interior
		// Leaf: index of the sole primitive inside primitives array
		int mOnePrimitive;
		// Leaf: this hold the pointer to the array of indexes of the primitives inside primitives array
		int* mPrimIndexes;
	};
private:
	union {
		// Both: low 2 bits to differentiate:
		// - interior nodes with x, y, z splits (value 0, 1, 2)
		// - leaf node (value 3)
		int mFlags;
		// Leaf: upper 30 bits
		// - how many primitives inside it
		int mPrimitivesCount;
		// Interior: upper 30 bits
		// - stores the index of the second node in the nodes array
		int mAboveChild;
	};
};