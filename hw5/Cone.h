#pragma once

#include "stdafx.h"
#include "Shape.h" 

class Cone
	: public Shape
{
public:
	Cone();
	Cone(const vec3 &center, float radius, float height);

	virtual bool intersect(const Ray& rRay, float& tHit, LocalGeo& local);
	virtual bool intersectP(const Ray& ray);
	virtual bool intersectP(const AABB& box);

	virtual void findUV(vec3 position, float& x, float& y);

	virtual AABB computeAABB() const;

private:
	vec3 getNormal(vec3& intersection);

	vec3 mCenter;          // The center of the cone
	float mRadius, mRadiusSquared;    // Radius
	float mHeight, mHeightSquared;    // Height
	float mConstantE;                 // E
};