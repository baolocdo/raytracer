#include "AggregatePrimitive.h"
#include "BRDF.h"
#include "Intersection.h"
#include "Primitive.h"

AggregatePrimitive::AggregatePrimitive(vector<Primitive*> primitives)
	: mPrimitives(primitives)
{
}

AggregatePrimitive::~AggregatePrimitive()
{
}

vector<Primitive*>& AggregatePrimitive::getPrimitives() {
	return mPrimitives;
}

AABB AggregatePrimitive::getAABB() const {
	return mAABB;
}