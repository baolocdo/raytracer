#include "Cylinder.h"
#include "Ray.h"
#include "LocalGeo.h"

using namespace std;

Cylinder::Cylinder()
{
	mTop    = vec3(0.0,2.0,0.0);
	mBottom = vec3(0.0,0.0,0.0);
	mPosition = vec3(0.0,1.0,0.0);
	mRadius = 1.0;
	mRadiusSquared = mRadius * mRadius;
}

Cylinder::Cylinder(vec3 position, float radius, vec3 top, vec3 bottom) 
	:mTop(top), mBottom(bottom), mPosition(position), mRadius(radius), mRadiusSquared(radius*radius)
{

}

bool Cylinder::intersect(const Ray &ray, float& tHit, LocalGeo& local)
{
	tHit = ray.getTMax();
	vec3 rayDirection = ray.getDirection();
	vec3 rayPosition = mPosition - ray.getPosition();	   

	vec3 intersection;

	float a = rayDirection.x*rayDirection.x + rayDirection.z*rayDirection.z;
	float b = rayPosition.x*rayDirection.x + rayPosition.z*rayDirection.z;
	float c = rayPosition.x*rayPosition.x + rayPosition.z*rayPosition.z - mRadiusSquared;

	float disc = b*b - a*c;

	tHit = INFINITY;
	float oldtHit = tHit;
	float d, tHit1, tHit2;

	int returnValue = 0;

	// If the discriminant is less than 0, then we totally miss the cylinder.
	if (disc > 0.0) {
		d = sqrt(disc);

		tHit2 = (b + d)/a;
		tHit1 = (b - d)/a;

		/*if (tHit2 < tHit1) {
		float temp = tHit1;
		tHit1 = tHit2;
		tHit2 = temp;
		}*/

		// If tHit2 < 0, then tHit1 is also < 0, so they are both miss.
		if (tHit2 >= 0) {
			// If tHit2 > 0, and tHit1 < 0, we are inside the cylinder.
			if (tHit1 < 0) {
				if (tHit2 < ray.getTMax()) { 
					tHit = tHit2; 
					returnValue = -1; 
				}
				// If tHit2 > 0, and tHit1 > 0, we hit the cylinder.
			} else if (tHit1 < tHit) { 
				tHit = tHit1; 
				returnValue = 1;
			}
		} else {
			return false;
		}

		intersection = ray.getPosition() + rayDirection*tHit ;

		/*	if (intersection.y > mTop.y || intersection.y < mBottom.y) {
		return false;
		}*/
		// Limit the y values
		if ((intersection.y > mTop.y) || (intersection.y < mBottom.y)) {
			tHit = tHit2;
			intersection = ray.getPosition() + rayDirection * tHit2 ;

			// Are we too high in our first hit, but hit the back wall later
			if ((intersection.y > mTop.y) || (intersection.y < mBottom.y)) {
				tHit = oldtHit; 
				returnValue = 0;
			} 
		}
		// Check to see if it hits the end planes
		// We know the normal of the end planes is y_min=y_max=(0,1,0)
		// Therefore the dot product is simply the y portion of the
		// We know the point Q = y_min or y_max
		// If the direction of the Ray is not parallel to the top or bottom...
		rayPosition = ray.getPosition();

		if (rayDirection.y != 0.0f) {
			// check top cap
			tHit1 = (mTop.y - rayPosition.y)/rayDirection.y;
			tHit2 = (mBottom.y - rayPosition.y)/rayDirection.y;

			if (tHit2 < tHit1) {
				float temp = tHit1;
				tHit1 = tHit2;
				tHit2 = temp;
			}

			intersection = ray.getPosition() + rayDirection * tHit1 - mPosition;

			if (intersection.x * intersection.x + intersection.z * intersection.z <= mRadiusSquared) {
				if (tHit1 < tHit) {
					tHit = tHit1; 
					returnValue = 2; 
				}
			} 
		} // end if rayDirection.y

		if (returnValue == 0 || tHit < ray.getTMin() || tHit > ray.getTMax())
			return false;

		intersection = ray.getPosition() + tHit * ray.getDirection();

		local.setPosition(intersection);
		local.setNormal(getNormal(intersection));

		return true;
	}

	return false;	
}

vec3 Cylinder::getNormal(vec3 &intersection) {
	if (intersection.y == mTop.y) {
		return vec3 (0.0f, 1.0f,0.0f); 
	}

	if (intersection.y == mBottom.y) { 
		return vec3 (0.0f, -1.0f, 0.0f); 
	}

	vec3 normal = (intersection - mPosition) / mRadius;

	normal.y = 0.0;

	return glm::normalize(normal);
}

bool Cylinder::intersectP(const Ray& ray) {

	vec3 rayDirection = ray.getDirection();
	vec3 rayPosition = mPosition - ray.getPosition();	   

	vec3 intersection;

	float a = rayDirection.x * rayDirection.x + rayDirection.z * rayDirection.z;
	float b = rayPosition.x * rayDirection.x + rayPosition.z * rayDirection.z;
	float c = rayPosition.x * rayPosition.x + rayPosition.z * rayPosition.z - mRadiusSquared;

	float disc = b * b - a * c;

	float d, tHit1, tHit2;
	float tHit = INFINITY;
	float oldtHit = tHit;

	int returnValue = 0;

	// If the discriminant is less than 0, then we totally miss the cylinder.
	if (disc > 0.0) {
		d = sqrt(disc);

		tHit2 = (b + d)/a;
		tHit1 = (b - d)/a;

		if (tHit2 < tHit1) {
			float temp = tHit1;
			tHit1 = tHit2;
			tHit2 = temp;
		}

		// If tHit2 < 0, then tHit1 is also < 0, so they are both miss.
		if (tHit2 >= 0) {
			// If tHit2 > 0, and tHit1 < 0, we are inside the cylinder.
			if (tHit1 < ray.getTMin()) {
				if (tHit2 < tHit) { 
					tHit = tHit2; 
					returnValue = -1; 
				}
				// If tHit2 > 0, and tHit1 > 0, we hit the cylinder.
			} else if (tHit1 < tHit) { 
				tHit = tHit1; 
				returnValue= 1;
			}
		} else {
			return false;
		}

		intersection = ray.getPosition() + rayDirection*tHit ;
		/*if (intersection.y > mTop.y || intersection.y < mBottom.y) {
		return false;
		}*/
		// Limit the y values
		if ((intersection.y > mTop.y) || (intersection.y < mBottom.y)) {
			intersection = ray.getPosition() + rayDirection * tHit2 ;

			// Are we too high in our first hit, but hit the back wall later
			if ((intersection.y > mTop.y) || (intersection.y < mBottom.y)) {
				tHit = oldtHit; 
				returnValue = 0;
			}
		}
		//     // Check to see if it hits the end planes
		//     // We know the normal of the end planes is y_min=y_max=(0,1,0)
		//     // Therefore the dot product is simply the y portion of the
		//     // We know the point Q = y_min or y_max
		//     // If the direction of the Ray is not parallel to the top or bottom...
		rayPosition = ray.getPosition();

		if (rayDirection.y != 0.0f) {
			// check top cap
			tHit1 = (mTop.y - rayPosition.y)/rayDirection.y;
			tHit2 = (mBottom.y - rayPosition.y)/rayDirection.y;

			if (tHit2 < tHit1) {
				float temp = tHit1;
				tHit1 = tHit2;
				tHit2 = temp;
			}

			intersection = ray.getPosition() + rayDirection * tHit1 - mPosition;

			if (intersection.x * intersection.x + intersection.z * intersection.z <= mRadiusSquared) {
				if (tHit1 < tHit) {
					tHit = tHit1; 
					returnValue = 2; 
				}
			} 
		} // end if rayDirection.y

		if (returnValue == 0 || tHit < ray.getTMin() || tHit > ray.getTMax())
			return false;

		return true;
	}

	return false;	
}

bool Cylinder::intersectP(const AABB& box) {
	return false;
}

void Cylinder::findUV(vec3 position, float& x, float& y) {

}

AABB Cylinder::computeAABB() const {
	return AABB();
}