#pragma once

#include "Shape.h"

class Triangle :
	public Shape
{
public:
	Triangle(vec3 vertice1, vec3 vertice2, vec3 vertice3);
	Triangle(vec3 vertice1, vec3 vertice2, vec3 vertice3,
		vec3 normal1, vec3 normal2, vec3 normal3);
	~Triangle(void);

	virtual bool intersect(const Ray& rRay, float& tHit, LocalGeo& local);
	virtual bool intersectP(const Ray& ray);
	virtual bool intersectP(const AABB& box);

	virtual void findUV(vec3 position, float& x, float& y);
	virtual AABB computeAABB() const;

	vec3 interpolateNormal(float beta, float gamma);

private:
	vec3 mVertice1, mVertice2, mVertice3;
	vec3 mNormal1, mNormal2, mNormal3;
};

