#include "BRDF.h"

BRDF::BRDF()
	: mShininess(0.0f)
{
	mKe = vec3(0.0);
	mKa = vec3(2.0, 2.0, 2.0);
	mKr = 1.0f;
	mKt = 0.0f;
	mIsGlossy = false;
	mGlossySamplingRate = 0;
	mRefractiveIndex = 1.0f;
	mGlossyDegreeOfBlur = 1;
}

BRDF::~BRDF(void)
{
}

vec3 BRDF::getKa() const {
	return mKa;
}

void BRDF::setKa(vec3 ka) {
	mKa = ka;
}

vec3 BRDF::getKd() const {
	return mKd;
}

void BRDF::setKd(vec3 kd) {
	mKd = kd;
}

vec3 BRDF::getKs() const {
	return mKs;
}

void BRDF::setKs(vec3 ks) {
	mKs = ks;
}

vec3 BRDF::getKe() const {
	return mKe;
}

void BRDF::setKe(vec3 ke) {
	mKe = ke;
}

float BRDF::getKr() const {
	return mKr;
}

void BRDF::setKr(float kr) {
	mKr = kr;
}

float BRDF::getShininess() const {
	return mShininess;
}

void BRDF::setShininess(float shininess) {
	mShininess = shininess;
}

float BRDF::getKt() const {
	return mKt;
}

void BRDF::setKt(float kt) {
	mKt = kt;
}

bool BRDF::isGlossy() const {
	return mIsGlossy;
}

void BRDF::setIsGlossy(bool isGlossy) { 
	mIsGlossy = isGlossy;
}

void BRDF::setRefractiveIndex(float index) {
	mRefractiveIndex = index;
}

float BRDF::getRefractiveIndex() const {
	return mRefractiveIndex;
}

int BRDF::getGlossySamplingRate() const {
	return mGlossySamplingRate;
}

void BRDF::setGlossySamplingRate(int samplingRate) {
	mGlossySamplingRate = samplingRate;
}

float BRDF::getGlossyDegreeOfBlur() const {
	return mGlossyDegreeOfBlur;
}

void BRDF::setGlossyDegreeOfBlur(float degreeOfBlur) {
	mGlossyDegreeOfBlur = degreeOfBlur;
}

BRDFType BRDF::type() const {
	return mType;
}

void BRDF::setType(BRDFType type) {
	mType = type;
}