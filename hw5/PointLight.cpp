#include "PointLight.h"
#include "AggregatePrimitive.h"
#include "Ray.h"
#include "Intersection.h"

PointLight::PointLight(void) 
	: mAttenuation(vec3(1.0f, 0.0f, 0.0f))
{
}

PointLight::PointLight(vec3 attenuation)
	: mAttenuation(attenuation)
{
}

PointLight::~PointLight(void) {
}

vec3 PointLight::getPosition() const {
	return mPosition;
}

void PointLight::setPosition(vec3 position) {
	mPosition = position;
}

void PointLight::generateLightRay(LocalGeo& local, Ray& lightRay) {
	lightRay.setDirection(glm::normalize(mPosition - local.getPosition()));
	lightRay.setPosition(local.getPosition());
	// world distance
	lightRay.setTMax(glm::length(mPosition - local.getPosition()));
}

vec3 PointLight::shading(LocalGeo& local, Material* material, Ray& lightRay, vec3 eyeDir) {
	float distance = lightRay.getTMax();
	float attenuation = (mAttenuation.x + mAttenuation.y * distance + mAttenuation.z * distance * distance);
	vec3 returnColor = computeLight(local, material, lightRay, eyeDir);
	return returnColor / attenuation;
}

vec3 PointLight::lightColor(LocalGeo& local, Material* material, AggregatePrimitive* primitives, Ray& ray) {
	Ray lightRay;
	generateLightRay(local, lightRay);

	if (glm::dot(local.getNormal(), lightRay.getDirection()) > 0.0f) {
		if (!primitives->intersectP(lightRay)) {
			return shading(local, material, lightRay, ray.getDirection());
		}			

	}
	return vec3(0.0f);
}