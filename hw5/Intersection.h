#pragma once

#include "LocalGeo.h"

class Primitive;

class Intersection
{
public:
	Intersection(void);
	~Intersection(void);

	Intersection(const LocalGeo& localGeo, Primitive* primitive);

	LocalGeo& getLocalGeo();
	void setLocalGeo(const LocalGeo& localGeo);

	Primitive* getPrimitive() const;
	void setPrimitive(Primitive* primitive);

private:
	LocalGeo mLocalGeo;
	Primitive* mPrimitive;
};

