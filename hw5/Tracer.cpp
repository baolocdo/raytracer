#include "Tracer.h"

Tracer::Tracer(AggregatePrimitive* primitives, vector<Light*> lights, int maxDepth, Environment* environment)
	: mPrimitives(primitives)
	, mLights(lights)
	, mMaxDepth(maxDepth)
	, mEnvironment(environment)
	, mCurrentRefractiveIndex(AIR_REFRACTIVE_INDEX)
{
}

Tracer::~Tracer(void)
{
}
