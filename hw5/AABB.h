#pragma once

#include "stdafx.h"

class AABB
{
public:
	AABB(void);
	AABB(vec3 min, vec3 max);
	AABB(vector<Primitive*> primitives);
	~AABB(void);

	void merge(AABB& box);
	void merge(vec3 point);

	void split(int axis, float position, AABB& leftBox, AABB& rightBox);
	bool intersectP(Ray ray, float& tmin, float& tmax);

	bool contains(vec3& point) const;
	bool contains(AABB box) const;

	vec3 getMinVector() const;
	void setMinVector(vec3 min);
	void setMinVectorComponent(int axis, float value);

	vec3 getMaxVector() const;
	void setMaxVector(vec3 max);
	void setMaxVectorComponent(int axis, float value);

	vec3 getCenter() const;
	vec3 getSize() const;
	vec3 getHalfSize() const;
	int getBiggestAxis() const;

	float volume() const;
	float surfaceArea() const;

private:
	vec3 mMin;
	vec3 mMax;
};

