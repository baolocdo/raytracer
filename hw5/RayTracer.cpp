#include "RayTracer.h"
#include "AggregatePrimitive.h"
#include "Light.h"
#include "Environment.h"
#include "Ray.h"
#include "Primitive.h"
#include "Intersection.h"
#include "Material.h"

RayTracer::RayTracer(AggregatePrimitive* primitives, vector<Light*> lights, int maxDepth, Environment* environment)
	: Tracer(primitives, lights, maxDepth, environment)
{
}

RayTracer::~RayTracer(void)
{
}

vec3 RayTracer::trace(Ray& ray, int depth) {
	float tHit = INFINITY;
	Intersection intersection;

	if (depth >= mMaxDepth) {
		return vec3(0.0f);
	}

	if (!mPrimitives->intersect(ray, tHit, intersection)) {
		// No intersection, return background color
		return mEnvironment->radiance(ray);
	}

	// Obtain the brdf at intersection point
	BRDF brdf = intersection.getPrimitive()->getBRDF(intersection.getLocalGeo());
	Material* material = intersection.getPrimitive()->getMaterial();

	vec3 color(0.0f);
	//vec3 lightColor(1.0f);
	if (depth == 0)
		color += brdf.getKa() + brdf.getKe();

	// There is an intersection, loop through all light source
	for (int i = 0; i < mLights.size(); i++) {
		color += mLights[i]->lightColor(intersection.getLocalGeo(), material, mPrimitives, ray);
	}

	vec3 reflectColor(0.0f);
	//Reflection
	if (brdf.getKr() > 0.0f && (glm::dot(intersection.getLocalGeo().getNormal(), ray.getDirection()) < 0)) {
		Ray reflectRay = createReflectRay(intersection.getLocalGeo(), ray);
		if (!brdf.isGlossy()) {
			reflectColor = brdf.getKr() * brdf.getKs() * trace(reflectRay, depth + 1);

			if (brdf.getKt() > 0.0f) {
				color += brdf.getKt() * dieletricRefract(ray, intersection, depth, reflectColor);
			} else {
				color += reflectColor;
			}
		} else {
			color += brdf.getKs() * findGlossyColor(brdf, reflectRay, depth)* brdf.getKr();
		}
	}

	return color;
}

Ray RayTracer::createReflectRay(LocalGeo& local, Ray& ray) {
	vec3 newDirection = (glm::normalize(ray.getDirection() - 2.0f * glm::dot(ray.getDirection(), local.getNormal()) * local.getNormal()));
	return Ray(local.getPosition(), newDirection);
}

bool RayTracer::createRefractionRay(vec3 normal, vec3 hitPosition, Ray& ray, float refractiveIndex1, float refractiveIndex2, Ray& refractionRay) {
	float refractiveIndex = refractiveIndex1 / refractiveIndex2;
	float cosI = glm::dot(normal, ray.getDirection());
	float sin2 = refractiveIndex * refractiveIndex * (1.0 - cosI * cosI);

	if (sin2 > 1.0) //Total internal reflection
		return false;

	vec3 newDirection = glm::normalize(refractiveIndex*(ray.getDirection() - normal * cosI) - normal * sqrt(1 - sin2));
	refractionRay.setDirection(newDirection);
	refractionRay.setPosition(hitPosition + newDirection * 0.001f);

	return true;
}

//GLOSSY:
vec3 RayTracer::findGlossyColor(BRDF& brdf, Ray& reflectRay, int depth) {
	vec3 color (0.0f);
	for (int i = 0; i < brdf.getGlossySamplingRate(); i++) {
		Ray randomReflectRay = createRandomReflectRay(reflectRay, brdf.getGlossyDegreeOfBlur());

		color += trace(randomReflectRay, depth+1);
	}

	return color/(float)brdf.getGlossySamplingRate();
}

Ray RayTracer::createRandomReflectRay(Ray& reflectRay, float mDegreeOfBlur) {
	//Sample randomly on the perpendicular plane to the primary reflected Ray
	vec3 randomVector = glm::normalize(vec3(-1+2*((float)rand())/RAND_MAX, -1+2*((float)rand())/RAND_MAX, -1+2*((float)rand())/RAND_MAX));
	vec3 randomVecOnPlane = (((float)rand()/(RAND_MAX+1))*mDegreeOfBlur)*glm::normalize(glm::cross(randomVector, reflectRay.getDirection()));

	vec3 centerOnPlane = reflectRay.getPosition() + reflectRay.getDirection();
	vec3 randomPointOnPlane = centerOnPlane + randomVecOnPlane;
	return Ray(reflectRay.getPosition(), glm::normalize(randomPointOnPlane - reflectRay.getPosition()));
}

vec3 RayTracer::dieletricRefract(Ray& ray, Intersection& intersection, int depth, vec3 reflColor)
{
	LocalGeo local = intersection.getLocalGeo();
	BRDF brdf = intersection.getPrimitive()->getBRDF(local);

	float index = brdf.getRefractiveIndex();

	/* Compute indices of refraction for (from, to) transmission media. */
	// CASE: From and object into open air.
	float n, nt;

	if (intersection.getPrimitive() == ray.getLastHitPrimitive()) {
		n = index;
		nt = AIR_REFRACTIVE_INDEX;
	} else {
		n = AIR_REFRACTIVE_INDEX;
		nt = index;
	}

	/* Now compute the refraction direction (returns false if total internal reflection). */
	Ray refractRay;
	bool refracted = createRefractedRay(ray, intersection, n, nt, refractRay);

	// Calculate Schlick approximation of Fresnel equations

	float schlick = schlickCalc(n, nt, ray.getDirection(), refractRay.getDirection(), local.getNormal());

	vec3 refractColor(0.0f);
	if (refracted) {
		// Trace refracted ray;
		refractRay.setLastHitPrimitive(intersection.getPrimitive());
		mCurrentRefractiveIndex = nt;
		refractColor = brdf.getKt() * trace(refractRay, depth + 1);
	}
	// Add up final reflection/refraction color contributions according to Fresnel
	return schlick*reflColor + (1-schlick)*refractColor;
}

bool RayTracer::dielectricCalc(Ray& ray, Intersection& intersection, Ray& refractRay, float& schlick)
{
	LocalGeo local = intersection.getLocalGeo();
	BRDF brdf = intersection.getPrimitive()->getBRDF(local);

	vec3 direction = ray.getDirection();

	vec3 normal = local.getNormal();
	bool into;

	// Checking whether ray is going into the object or going out.
	// Negative dot product means going in, positive means going out (so we need to look at flipped normal)
	float cosAngle = glm::dot(normal, direction);
	if (cosAngle > 0) {
		into = false;
	} else {
		into = true;
	}

	float index = brdf.getRefractiveIndex();

	/* Compute indices of refraction for (from, to) transmission media. */
	// CASE: From an object into open air.
	float n, nt;
	if (!into/*ray.getLastHitPrim() == intersection.primitive*/)
	{
		n = index;
		nt = 1.0;
	}
	// CASE: From open air into an object.
	else
	{
		n = 1.0;
		nt = index;
	}

	/* Now compute the refraction direction (returns false if total internal reflection). */
	bool refracted = createRefractedRay(ray, intersection, n, nt, refractRay);

	// Calculate Schlick approximation of Fresnel equations

	schlick = schlickCalc(n, nt, ray.getDirection(), refractRay.getDirection(), local.getNormal());

	return refracted;
}


bool RayTracer::createRefractedRay(Ray& ray, Intersection& intersection, float oldIndex, float newIndex, Ray& refractRay) {
	float n = oldIndex/newIndex;
	vec3 direction = ray.getDirection();

	LocalGeo local = intersection.getLocalGeo();

	vec3 normal = local.getNormal();

	// Checking whether ray is going into the object or going out.
	// Negative dot product means going in, positive means going out (so we need to look at flipped normal)
	float cosAngle = glm::dot(direction, normal);

	if (cosAngle  > 0) {
		normal = -normal;
	}

	float c = glm::dot(direction, normal);
	float cosPhi2 = (1 - ((n * n) * (1 - (c * c))));

	// If cos(phi)^2 is less than 0, then no refraction ray exists and all 
	// the energy is reflected (TOTAL INTERNAL REFLECTION).
	vec3 refractDirection;

	if (cosPhi2 < 0) 
		return false;
	else {
		float cosPhi = sqrt(cosPhi2);
		vec3 term1 = n * (direction - normal * (c));
		refractDirection = term1 - normal * cosPhi;

		refractRay.setDirection(glm::normalize(refractDirection));
		refractRay.setPosition(local.getPosition());

		return true;
	}
}

float RayTracer::schlickCalc(float index1, float index2, vec3& rayDirection, vec3& refractDirection, vec3& normal)
{
	float bigIndex = glm::max(index1, index2); 
	float smallIndex = glm::min(index1, index2);

	float R0 = ((bigIndex - smallIndex)/(bigIndex + smallIndex)); 

	R0 = R0*R0;

	bool into = glm::dot(rayDirection, normal) < 0;

	float c = 1 - (into ? (-glm::dot(rayDirection, normal)) : (glm::dot(refractDirection, normal)));
	return R0 + (1-R0)* c * c * c * c * c;
}
