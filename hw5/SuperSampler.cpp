#include "SuperSampler.h"
#include "Ray.h"
#include "Tracer.h"

SuperSampler::SuperSampler(int width, int height, int samplingRate, float startTime, float endTime)
	: Sampler(width, height), mSamplingRate(samplingRate), mStartTime(startTime), mEndTime(endTime)
{
	dx = 2.0f / (mWidth * mSamplingRate);
	dy = 2.0f / (mHeight * mSamplingRate);
}

SuperSampler::~SuperSampler(void)
{
}

vec3 SuperSampler::sample(int x0, int y0, Tracer* rayTracer, Camera* camera) {
	vec3 color(0.0f);
	float x = ((float) x0 / mWidth - 0.5f) * 2;
	float y = ((float) y0 / mHeight - 0.5f) * 2;
	float time = mStartTime;

	if (mSamplingRate <= 1 && !camera->isDepthOfField()) {
		float randomTime = rand() / (float)RAND_MAX * (mEndTime - mStartTime); 
		Ray newRay = camera->generateRay(x + 0.5*dx, y + 0.5*dy);
		newRay.setTime(randomTime);
		color = rayTracer->trace(newRay, 0);
		/*color += rayTracer->getPixelColor(camera->generateRay(x + 0.5*dx, y + 0.5*dy));
		color /= 2.0f;*/
	} else {
		for (int i = 0; i < mSamplingRate; i++) {
			for (int j = 0; j < mSamplingRate; j++) {
				//Monte Carlo antialiasing
				float randomTime = rand() / (float)RAND_MAX * (mEndTime - mStartTime); 
				
				float rx = rand() / (float)RAND_MAX / 2; //Random from 0 to 0.5
				float ry = rand() / (float)RAND_MAX / 2; //Random from 0 to 0.5

				Ray newRay = camera->generateRay(x + (i + rx) * dx, y + (j + ry) * dy);
				newRay.setTime(randomTime);

				color += rayTracer->trace(newRay, 0);
				//color += rayTracer->getPixelColor(camera->generateRay(x + (i + rx) * dx, y + (j + ry) * dy));
			}
		}

		color /= (mSamplingRate * mSamplingRate);
	}

	if (camera->isDepthOfField()) {
		Ray primaryRay = camera->generateRay(x , y);
		vec3 currentFocalPoint = camera->findCurrentFocalPoint(x, y, primaryRay);
		int n = camera->getApertureSize();

		int rayNum = camera->getSamplingRate();
		float dx = ((float)n / (rayNum*mWidth));
		float dy = ((float)n / (rayNum*mHeight)); 
		float row = x - (float) n/mWidth/2;
		float col = y - (float) n/mHeight/2;
		float sampleX, sampleY;

		for (int i = 0; i < rayNum; i++) {
			sampleX = row + i*dx;
			if (sampleX <= 1.0f && sampleX >=-1.0f) {
				for (int j = 0; j < rayNum; j++) {
					sampleY = col + j*dy;
					if (sampleY <= 1.0f && sampleY >=-1.0f) {
						color += rayTracer->trace(camera->generateRayDOF(sampleX, sampleY, currentFocalPoint), 0);
					}
				}
			}
		}
		color /= (rayNum*rayNum);
	}

	/*float exposure = -1.00f;
	color.x = 1.0f - expf(color.x * exposure);
	color.y = 1.0f - expf(color.y * exposure);
	color.z = 1.0f - expf(color.z * exposure);*/
	if (color.x > 1.0f)
		color.x = 1.0f;

	if (color.y > 1.0f)
		color.y = 1.0f;

	if (color.z > 1.0f)
		color.z = 1.0f;

	return color;
}