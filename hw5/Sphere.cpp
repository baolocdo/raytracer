#include "Sphere.h"
#include "LocalGeo.h"
#include "Ray.h"
#include "Primitive.h"

Sphere::Sphere(vec3 center, float radius) {
	mCenter = center;
	mRadius = radius;
}

Sphere::~Sphere(void)
{
}

vec3 Sphere::getCenter() const {
	return mCenter;
}

void Sphere::setCenter(vec3 center) {
	// BUG: potential bug, AABB is not recalculated
	mCenter = center;
}

float Sphere::getRadius() const {
	return mRadius;
}

void Sphere::setRadius(float radius) {
	mRadius = radius;
}

bool Sphere::intersect(const Ray& ray, float& tHit, LocalGeo& local) {
	//Compute A, B and C coefficients
	vec3 eyeToCenter = ray.getPosition() - mCenter;
	float a = glm::dot(ray.getDirection(), ray.getDirection());
	float b = 2.0f * glm::dot(ray.getDirection(), eyeToCenter);
	float c = glm::dot(eyeToCenter, eyeToCenter) - (mRadius * mRadius);

	//Find discriminant
	float disc = b * b - 4 * a * c;

	// if discriminant is negative there are no real roots, so return 
	// false as ray misses sphere
	if (disc < 0)
		return false;

	// compute q as described above
	float distSqrt = sqrtf(disc);
	float q;
	if (b < 0)
		q = (-b - distSqrt) / 2.0f;
	else
		q = (-b + distSqrt) / 2.0f;

	// compute t0 and t1
	float t0 = q / a;
	float t1 = c / q;

	// make sure t0 is smaller than t1
	if (t0 > t1)
	{
		// if t0 is bigger than t1 swap them around
		float temp = t0;
		t0 = t1;
		t1 = temp;
	}

	// if t1 is less than zero, the object is in the ray's negative direction
	// and consequently the ray misses the sphere
	if (t0 < ray.getTMin() || t0 > ray.getTMax() || (t0 < ray.getTMin() &&  t1 > ray.getTMax()))
		return false;

	// if t0 is less than zero, the intersection point is at t1
	if (t0 < ray.getTMin())
	{
		tHit = t1;
		local.setPosition(ray.getPosition() + tHit * ray.getDirection());
		local.setNormal(glm::normalize(local.getPosition() - mCenter));
		return true;
	}
	// else the intersection point is at t0
	else
	{
		tHit = t0;
		local.setPosition(ray.getPosition() + tHit * ray.getDirection());
		local.setNormal(glm::normalize(local.getPosition() - mCenter));
		return true;
	}

	return false;
}

bool Sphere::intersectP(const Ray& ray) {
	vec3 eyeToCenter = ray.getPosition() - mCenter;
	float a = glm::dot(ray.getDirection(), ray.getDirection());
	float b = 2 * glm::dot(ray.getDirection(), eyeToCenter);
	float c = glm::dot(eyeToCenter, eyeToCenter) - (mRadius * mRadius);

	//Find discriminant
	float disc = b * b - 4 * a * c;

	// if discriminant is negative there are no real roots, so return 
	// false as ray misses sphere
	if (disc < 0)
		return false;

	// compute q as described above
	float distSqrt = sqrtf(disc);
	float q;
	if (b < 0)
		q = (-b - distSqrt)/2.0;
	else
		q = (-b + distSqrt)/2.0;

	// compute t0 and t1
	float t0 = q / a;
	float t1 = c / q;

	// make sure t0 is smaller than t1
	if (t0 > t1)
	{
		// if t0 is bigger than t1 swap them around
		float temp = t0;
		t0 = t1;
		t1 = temp;
	}

	// if t1 is less than zero, the object is in the ray's negative direction
	// and consequently the ray misses the sphere
	//t0 has to be bigger than 0.1 to avoid self-shadowing 
	if (t0 < ray.getTMin() || t0 > ray.getTMax() || (t0 < ray.getTMin() &&  t1 > ray.getTMax()))
		return false;

	return true;
}

bool Sphere::intersectP(const AABB& box) {
	// TODO: implement
	return false;
}

void Sphere::findUV(vec3 position, float& u, float& v) {
	float theta = acos((position.z - mCenter.z)/ mRadius);
	float phi = atan2(position.y - mCenter.y, position.x - mCenter.x);
	u = phi / (2*PI);
	v = (PI - theta)/PI;
}

AABB Sphere::computeAABB() const {
	AABB box;
	box.merge(mCenter + vec3(mRadius, mRadius, mRadius));
	box.merge(mCenter - vec3(mRadius, mRadius, mRadius));
	return box;
}
