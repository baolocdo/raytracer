#include "GeometricPrimitive.h"
#include "Material.h"
#include "BRDF.h"
#include "Shape.h"
#include "Intersection.h"
#include "Ray.h"
//#include "Transform.h"

GeometricPrimitive::GeometricPrimitive(Shape* shape, Material* material, Transformation transformation, vec3 velocity)
	: mShape(shape), mMaterial(material), mObjToWorld(transformation), mWorldToObj(transformation.getInverse()), mVelocity(velocity)
{
}

GeometricPrimitive::~GeometricPrimitive(void)
{
}

Transformation GeometricPrimitive::getObjToWorld() const {
	return mObjToWorld;
}

void GeometricPrimitive::setObjToWorld(Transformation objToWorld) {
	mObjToWorld = objToWorld;
}

Transformation GeometricPrimitive::getWorldToObj() const {
	return mWorldToObj;
}

void GeometricPrimitive::setWorldToObj(Transformation worldToObj) {
	mWorldToObj = worldToObj;
}

bool GeometricPrimitive::intersect(const Ray& ray, float& tHit, Intersection& intersection) {  
	vec3 translate = mVelocity * ray.getTime();
	Transformation objToWorldUpdate = Transformation(glm::translate(mObjToWorld.getTransform(), translate));
	Transformation worldToObjUpdate = Transformation(objToWorldUpdate.getInverse());

	Ray newRay = worldToObjUpdate * ray;

	//Ray newRay = (mWorldToObj) * (ray);

	LocalGeo local;        

	bool isHit = mShape->intersect(newRay, tHit, local);

	if (isHit) {
		intersection.setPrimitive(this);
		intersection.setLocalGeo(objToWorldUpdate * local);
		//intersection.setLocalGeo(mObjToWorld * local);

		//Added for texture
		float u;
		float v;
		mShape->findUV(local.getPosition(), u, v);
		intersection.getLocalGeo().setUVCoordinate(u, v);
	}

	return isHit;                            
}

bool GeometricPrimitive::intersectP(const Ray& ray) {
	vec3 translate = mVelocity * ray.getTime();
	Transformation objToWorldUpdate (glm::translate(mObjToWorld.getTransform(), translate));
	Transformation worldToObjUpdate = Transformation(objToWorldUpdate.getInverse());

	Ray newRay = worldToObjUpdate * ray;
	//Ray newRay = (mWorldToObj) * (ray);
	newRay.setTMax(ray.getTMax());
	return mShape->intersectP(newRay);                        
}

bool GeometricPrimitive::intersectP(const AABB& box) {
	return mShape->intersectP(box);
}

BRDF GeometricPrimitive::getBRDF(const LocalGeo& local) {
	return mMaterial->getBRDF(local);
}

AABB GeometricPrimitive::getAABB() const {
	return mObjToWorld * mShape->computeAABB();
}

Material* GeometricPrimitive::getMaterial() const{
	return mMaterial;
}