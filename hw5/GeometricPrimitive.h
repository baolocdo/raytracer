#pragma once

#include "stdafx.h"
#include "Primitive.h"
#include "Transformation.h"

class GeometricPrimitive
	: public Primitive
{
public:
	GeometricPrimitive(Shape* shape, Material* material, Transformation transformation, vec3 velocity = vec3(0.0f));
	~GeometricPrimitive(void);

	Transformation getObjToWorld() const;
	void setObjToWorld(Transformation objToWorld);

	Transformation getWorldToObj() const;
	void setWorldToObj(const Transformation worldToObj);

	virtual bool intersect(const Ray& ray, float& thit, Intersection& intersection);     
	virtual bool intersectP(const Ray& ray);
	virtual bool intersectP(const AABB& box);
	virtual BRDF getBRDF(const LocalGeo& local);
	virtual AABB getAABB() const;

	virtual Material* getMaterial() const;
	//virtual void findUV(vec3 position, float* u, float *v) = 0;

private:
	vec3 mVelocity;

	Transformation mObjToWorld;
	Transformation mWorldToObj;
	Shape* mShape;
	Material* mMaterial;
};

