#include "NaiveAggregatePrimitive.h"
#include "Intersection.h"
#include "Ray.h"
#include "Primitive.h"

NaiveAggregatePrimitive::NaiveAggregatePrimitive(vector<Primitive*> primitives)
	: AggregatePrimitive(primitives)
{
	mAABB = AABB(primitives);
}

NaiveAggregatePrimitive::~NaiveAggregatePrimitive(void)
{
}

bool NaiveAggregatePrimitive::intersect(const Ray& ray, float& tHit, Intersection& intersection) {
	Intersection closestIntersection;
	float t;
	bool isHit = false;

	for (int i = 0; i < mPrimitives.size(); i++) {
		if (mPrimitives[i]->intersect(ray, t, closestIntersection)) {
			if (tHit > t) {
				isHit = true;
				tHit = t;
				intersection.setPrimitive(closestIntersection.getPrimitive());
				intersection.setLocalGeo(closestIntersection.getLocalGeo());
			}
		}
	}

	return isHit;
}

bool NaiveAggregatePrimitive::intersectP(const Ray& ray) {
	for (int i = 0; i < mPrimitives.size(); i++) {
		if (mPrimitives[i]->intersectP(ray))
			return true;
	}

	return false;
}
