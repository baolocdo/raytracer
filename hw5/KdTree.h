#pragma once

#include "stdafx.h"
#include "AggregatePrimitive.h"

#define MAX_TODO 64

struct BoundEdge {
	BoundEdge() {
	}

	BoundEdge(float tt, int pn, bool starting) {
		t = tt;
		primNum = pn;
		type = starting ? START : END;
	}

	bool operator < (const BoundEdge& e) const {
		if (t == e.t) {
			return (int) type < (int) e.type;
		} else {
			return t < e.t;
		}
	}

	// public members:
	float t;
	int primNum;
	enum {
		START, END
	} type;
};

struct KdToDo {
	const KdNode* node;
	float tmin, tmax;
};

class KdTree
	: public AggregatePrimitive
{
public:
	// public methods
	KdTree(vector<Primitive*> primitives,
		float intersectCost, float traversalCost, float emptyBonus,
		int maxPrimitivesPerNode, int maxDepth);
	~KdTree(void);

	void buildTree(int nodeNum, const AABB& box, int* primIndexes, 
		int primitivesCount, int depth, BoundEdge* edges[3], int* prims0, int* prims1);

	//void build(Scene* scene);

	//void addPrimitive(Primitive* p);
	//vector<Primitive*>& getPrimitives();
	//void setPrimitives(vector<Primitive*> value);

	virtual bool intersect(const Ray& ray, float& tHit, Intersection& intersection);
	virtual bool intersectP(const Ray& ray);
	//bool intersectKd(Ray& ray, float& tHit, Intersection& intersection, KdTree* kdTree);
	//
	//KdNode* getRootNode() const;
	//void setRootNode(KdNode* root);

	//void subdivide(KdNode*, AABB& box, int depth);

private:
	// private methods
	vector<AABB> mPrimitiveAABBs;

	KdNode* mNodes;
	int mAllocedNodesCount;
	int mNextFreeNode;

	float mIntersectCost; // default 80
	float mTraveralCost; // default 1
	float mEmptyBonus;
	int mMaxPrimitivesPerNode;
	int mMaxDepth;
};