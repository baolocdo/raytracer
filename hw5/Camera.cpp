#include "Camera.h"
#include "Ray.h"

Camera::Camera(vec3 position, vec3 lookAt, vec3 up, float fovy, int width, int height)
	: mPosition(position), mLookAt(lookAt), mUp(up)
{
	mAspect = (float)width / height;
	mCameraToWorldTransformation = Transformation(glm::inverse(glm::lookAt(position, lookAt, up)));
	setFovY(glm::radians(fovy));
	mIsDepthOfField = false;
	mDirection = glm::normalize(mLookAt - mPosition);
	mDistance = glm::length(mLookAt - mPosition);
}

Camera::~Camera(void) {
}

vec3 Camera::getPosition() const {
	return mPosition;
}

void Camera::setPosition(vec3 lookFrom) {
	mPosition = lookFrom;
	mCameraToWorldTransformation = Transformation(glm::inverse(glm::lookAt(mPosition, mLookAt, mUp)));
}

vec3 Camera::getLookAt() const {
	return mLookAt;
}

void Camera::setLookAt(vec3 lookAt) {
	mLookAt = lookAt;
	mCameraToWorldTransformation = Transformation(glm::inverse(glm::lookAt(mPosition, mLookAt, mUp)));
}

vec3 Camera::getUp() const {
	return mUp;
}

void Camera::setUp(vec3 up) {
	mUp = up;
	mCameraToWorldTransformation = Transformation(glm::inverse(glm::lookAt(mPosition, mLookAt, mUp)));
}

float Camera::getFovY() const {
	return mFovY;
}

void Camera::setFovY(float fov) {
	mFovY = fov;
	mTanFovY = glm::tan(mFovY / 2.0f);
	mTanFovX = mTanFovY * mAspect;
}

void Camera::setApertureSize(int apertureSize) {
	mApertureSize = apertureSize;
}

int Camera::getApertureSize() {
	return mApertureSize;
}

void Camera::setFocalLength(int focalLength) {
	mFocalLength = focalLength;
	setDOFConstant();
}

int Camera::getFocalLength() {
	return mFocalLength;
}

void Camera::setDOFConstant() {
	mConstant = (mDistance + mFocalLength)/mDistance ;
}


void Camera::setSamplingRate(int samplingRate) {
	mSamplingRate = samplingRate;
}

int Camera::getSamplingRate() const {
	return mSamplingRate;
}

bool Camera::isDepthOfField() const {
	return mIsDepthOfField;
}

void Camera::setDepthOfField(bool isDepthOfField) {
	mIsDepthOfField = isDepthOfField;
}

vec3 Camera::findCurrentFocalPoint(float x, float y, Ray& primaryRay) {
	vec3 pointOnScreenPosition = vec3(mCameraToWorldTransformation.getTransform() * vec4(x * mTanFovX, y * mTanFovY, -1, 1));

	float distanceFromEyeToScreen = glm::length(pointOnScreenPosition - mPosition) * mConstant;
	vec3 pointIntersectOnFocalPlane = mPosition + distanceFromEyeToScreen * primaryRay.getDirection();

	//Find a focal point on the focal plane
	return pointIntersectOnFocalPlane;
}

Ray Camera::generateRay(float x, float y) {
	// Assume that the screen is at (0, 0, -1) and the camera is at the origin
	// the sample is then at (x, y, -1)
	// the transformed sample position is:
	vec3 samplePosition = vec3(mCameraToWorldTransformation.getTransform() * vec4(x * mTanFovX, y * mTanFovY, -1, 1));

	//the eye position is at origin. Need to transform back to the Scene
	vec3 direction = glm::normalize(samplePosition - mPosition); 

	return Ray(mPosition, direction);

}

Ray Camera::generateRayDOF(float x, float y, vec3 currentFocalPoint) {
	//Not looking from the eye any more
	//Now we are looking from the screen plane
	vec3 samplePosition = vec3(mCameraToWorldTransformation.getTransform() * vec4(x * mTanFovX, y * mTanFovY, -1, 1));
	vec3 direction = glm::normalize(currentFocalPoint - samplePosition);
	return Ray(samplePosition, direction);	
}