// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include <GL/glew.h>
#include <GL/glut.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <string>
#include <vector>
#include <iostream>

#include <limits>

using namespace std;

#define INFINITY std::numeric_limits<float>::infinity()
#define FLOAT_EQUAL(a,b) (fabs(a - b) < std::numeric_limits<float>::epsilon())

const float PI = 3.14159265f;
const float AIR_REFRACTIVE_INDEX = 1.0f;

typedef glm::mat3 mat3;
typedef glm::mat4 mat4; 
typedef glm::vec2 vec2; 
typedef glm::vec3 vec3; 
typedef glm::vec4 vec4; 

class AABB;
class AggregatePrimitive;
class AreaLight;
class Box;
class BRDF;
class Camera;
class Cone;
class ConstantEnvironment;
class Cylinder;
class DirectionalLight;
class Environment;
class FileTokenizer;
class Film;
class GeometricPrimitive;
class HDREnvironment;
class Intersection;
class KdNode;
class KdTree;
class Light;
class LocalGeo;
class Material;
class NaiveAggregatePrimitive;
class PointLight;
class Primitive;
class Quad;
class Ray;
class RayTracer;
class Sampler;
class Scene;
class Shape;
class Sphere;
class SphereLight;
class SuperSampler;
class Texture;
class Tracer;
class Transform;
class Transformation;
class Triangle;
class Utils;