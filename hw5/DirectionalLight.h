#pragma once

#include "stdafx.h"
#include "Light.h"

class DirectionalLight
	: public Light
{
public:
	DirectionalLight(void);
	~DirectionalLight(void);

	virtual void generateLightRay(LocalGeo& local, Ray& lightRay);
	virtual vec3 shading(LocalGeo& local, Material* material, Ray& lightRay, vec3 eyeDir);

	vec3 getDirection() const;
	void setDirection(vec3 direction);

	virtual vec3 lightColor(LocalGeo& local, Material* material, AggregatePrimitive* primitives, Ray& ray);

private:
	vec3 mDirection;
};

