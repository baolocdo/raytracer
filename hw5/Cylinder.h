#pragma once

#include "Shape.h"

class Cylinder
	: public Shape
{
public:
	Cylinder(void);
	~Cylinder(void);

	Cylinder(vec3 position, float radius, vec3 top, vec3 bottom);

	virtual bool intersect(const Ray& rRay, float& tHit, LocalGeo& local);
	virtual bool intersectP(const Ray& ray);
	virtual bool intersectP(const AABB& box);

	virtual void findUV(vec3 position, float& x, float& y);

	virtual AABB computeAABB() const;

private:
	vec3 getNormal(vec3& intersection);

	vec3 mTop, mBottom;
	float mRadius;
	vec3 mPosition;
	float mRadiusSquared;
	//float mPhiMax;
};

