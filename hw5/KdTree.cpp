#include "KdTree.h"
#include "Ray.h"
#include "Intersection.h"
#include "Primitive.h"
#include "KdNode.h"

#include <algorithm>

KdTree::KdTree(vector<Primitive*> primitives,
	float intersectCost, float traversalCost, float emptyBonus,
	int maxPrimitivesPerNode, int maxDepth)
	: AggregatePrimitive(primitives)
	, mIntersectCost(intersectCost)
	, mTraveralCost(traversalCost)
	, mEmptyBonus(emptyBonus)
	, mMaxPrimitivesPerNode(maxPrimitivesPerNode)
	, mMaxDepth(maxDepth)
{
	int primitivesCount = mPrimitives.size();

	mAllocedNodesCount = 8;
	mNodes = new KdNode[mAllocedNodesCount];
	mNextFreeNode = 0;

	// build kd-tree for accelerator
	// refine primitives
	// build Kd Tree
	if (mMaxDepth <= 0) {
		mMaxDepth = (int) glm::round(8.0f + 1.3f * glm::log2((float) primitivesCount));
	}

	mAABB = mPrimitives[0]->getAABB();

	// compute bounds for kd-tree construction
	mPrimitiveAABBs.reserve(primitivesCount);
	for (int i = 0; i < primitivesCount; i++) {
		AABB box = mPrimitives[i]->getAABB();
		mAABB.merge(box);
		mPrimitiveAABBs.push_back(box);
	}

	// DONE
	// allocate working memory for kd tree construction
	BoundEdge* edges[3];
	edges[0] = new BoundEdge[2 * primitivesCount];
	edges[1] = new BoundEdge[2 * primitivesCount];
	edges[2] = new BoundEdge[2 * primitivesCount];

	int* primIndexesBelow = new int[primitivesCount];
	int* primIndexesAbove = new int[(mMaxDepth + 1) * primitivesCount];

	// initialize primIndexes
	int* primIndexes = new int[primitivesCount];
	for (int i = 0; i < primitivesCount; i++) {
		primIndexes[i] = i;
	}

	// start recursive construction
	buildTree(0, mAABB, primIndexes, primitivesCount, mMaxDepth, edges, primIndexesBelow, primIndexesAbove);

	// free working memory for kd tree construction
	delete[] primIndexesBelow;
	delete[] primIndexesAbove;
	delete[] primIndexes;
	delete[] edges[0];
	delete[] edges[1];
	delete[] edges[2];
}

KdTree::~KdTree(void)
{
}

void KdTree::buildTree(int nodeIndex, const AABB& box, int* primIndexes, int primitivesCount, int depth, BoundEdge* edges[3], int* primIndexesBelow, int* primIndexesAbove) {
	// get next free mNodes[nodeIndex] from nodes array
	if (mNextFreeNode >= mAllocedNodesCount) {
		int allocCount = 2 * mAllocedNodesCount;

		KdNode* nodes = new KdNode[allocCount];
		memcpy(nodes, mNodes, mAllocedNodesCount * sizeof(KdNode));
		delete[] (mNodes);

		mNodes = nodes;
		mAllocedNodesCount = allocCount;
	}
	mNextFreeNode++;

	// initialize leaf mNodes[nodeIndex] if termination criteria met
	if (primitivesCount <= mMaxPrimitivesPerNode || depth == 0) {
		mNodes[nodeIndex].initLeaf(primIndexes, primitivesCount);
		return;
	}

	// initialize interior mNodes[nodeIndex] and continue recursion
	// choose split axis position for interior mNodes[nodeIndex]
	int bestAxis = -1;
	int bestEdgeIndex = -1;
	float bestCost = INFINITY;
	float oldCost = mIntersectCost * (float)primitivesCount;
	float totalSurfaceArea = box.surfaceArea();
	float invertTotalSurfaceArea = 1.0f / totalSurfaceArea;
	vec3 boxSize = box.getSize();
	// choose which axis to split along
	int axis = box.getBiggestAxis();
	int retries = 0;

retrySplit:
	// initialize edges for axis
	for (int i = 0; i < primitivesCount; i++) {
		int primIndex = primIndexes[i];
		const AABB& aabb = mPrimitiveAABBs[primIndex];
		edges[axis][2 * i] = BoundEdge(aabb.getMinVector()[axis], primIndex, true);
		edges[axis][2 * i + 1] = BoundEdge(aabb.getMaxVector()[axis], primIndex, false);
	}
	sort(&edges[axis][0], &edges[axis][2 * primitivesCount]);

	// compute cost of all splits for axis to find best
	int primitiveCountBelow = 0;
	int primitiveCountAbove = primitivesCount;

	for (int i = 0; i < 2 * primitivesCount; i++) {
		if (edges[axis][i].type == BoundEdge::END) {
			primitiveCountAbove--;
		}

		float edgeT = edges[axis][i].t;
		if (edgeT > box.getMinVector()[axis] && edgeT < box.getMaxVector()[axis]) {
			// compute cost for split at ith edge
			int secondAxis = (axis + 1) % 3;
			int thirdAxis = (axis + 2) % 3;

			float surfaceAreaBelow = 2 * (boxSize[secondAxis] * boxSize[thirdAxis] + (edgeT - box.getMinVector()[axis]) * (boxSize[secondAxis] + boxSize[thirdAxis]));
			float surfaceAreaAbove = 2 * (boxSize[secondAxis] * boxSize[thirdAxis] + (box.getMaxVector()[axis] - edgeT) * (boxSize[secondAxis] + boxSize[thirdAxis]));

			float emptyBonus = (primitiveCountAbove == 0 || primitiveCountBelow == 0) ? mEmptyBonus : 0.0f;

			float cost = mTraveralCost + mIntersectCost * (1.0f - emptyBonus) 
				* ((surfaceAreaBelow * invertTotalSurfaceArea) * primitiveCountBelow + (surfaceAreaAbove * invertTotalSurfaceArea) * primitiveCountAbove);

			// update best split if this is lowest cost so far
			if (cost < bestCost) {
				bestCost = cost;
				bestAxis = axis;
				bestEdgeIndex = i;
			}
		}

		if (edges[axis][i].type == BoundEdge::START) {
			primitiveCountBelow++;
		}
	}

	// create leaf if no good splits were found
	if (bestAxis == -1 && retries < 2) {
		retries++;
		axis = (axis + 1) % 3;
		goto retrySplit;
	}

	// TODO: what is this
	//if (bestCost > oldCost) {
	//	//++badRefines;
	//}

	if ((bestCost > oldCost)
		||(bestCost > 4.0f * oldCost && primitivesCount < 16)
		|| bestAxis == -1) {
			mNodes[nodeIndex].initLeaf(primIndexes, primitivesCount);
			return;
	}

	// classify primitives with respect to split
	int n0 = 0, n1 = 0;
	for (int i = 0; i < bestEdgeIndex; i++) {
		if (edges[bestAxis][i].type == BoundEdge::START) {
			primIndexesBelow[n0++] = edges[bestAxis][i].primNum;
		}
	}
	for (int i = bestEdgeIndex + 1; i < 2 * primitivesCount; i++) {
		if (edges[bestAxis][i].type == BoundEdge::END) {
			primIndexesAbove[n1++] = edges[bestAxis][i].primNum;
		}
	}

	// recursively initialize children nodes
	float tsplit = edges[bestAxis][bestEdgeIndex].t;

	AABB box0 = box;
	AABB box1 = box;

	box0.setMaxVectorComponent(bestAxis, tsplit);
	box1.setMinVectorComponent(bestAxis, tsplit);

	buildTree(nodeIndex + 1, box0, primIndexesBelow, n0, depth - 1, edges, primIndexesBelow, primIndexesAbove + primitivesCount);

	mNodes[nodeIndex].initInterior(bestAxis, mNextFreeNode, tsplit);

	buildTree(mNextFreeNode, box1, primIndexesAbove, n1, depth - 1, edges, primIndexesBelow, primIndexesAbove + primitivesCount);
}

bool KdTree::intersect(const Ray& ray, float& tHit, Intersection& intersection) {
	float tmin, tmax;
	if (!mAABB.intersectP(ray, tmin, tmax)) {
		return false;
	}

	// prepare to traverse kd tree for ray
	vec3 invDir(1.0f / ray.getDirection().x, 1.0f / ray.getDirection().y, 1.0f / ray.getDirection().z);
	KdToDo todo[MAX_TODO];
	int todoPos = 0;

	// traver kd tree nodes in order for ray
	bool hit = false;
	const KdNode* node = &mNodes[0];

	while (node != NULL) {
		// bail out if we found a hit closer than the current node
		if (ray.getTMax() < tmin) {
			break;
		}

		if (!node->isLeaf()) {
			// process kd tree interior node

			// compute parametric distance along ray to split plane
			int axis = node->getSplitAxis();
			float tPlane = (node->getSplitPosition() - ray.getPosition()[axis]) * invDir[axis];

			// get node children pointers for ray
			const KdNode* firstChild;
			const KdNode* secondChild;
			bool belowFirst = (ray.getPosition()[axis] < node->getSplitPosition())
				|| (ray.getPosition()[axis] == node->getSplitPosition() && ray.getDirection()[axis] < 0);

			if (belowFirst) {
				firstChild = node + 1;
				secondChild = &mNodes[node->getSecondChildNodeIndex()];
			} else {
				firstChild = &mNodes[node->getSecondChildNodeIndex()];
				secondChild = node + 1;
			}

			// advance to next child node, possibly enqueue other child
			if (tPlane > tmax || tPlane <= 0.0f) {
				node = firstChild;
			} else if (tPlane < tmin) {
				node = secondChild;
			} else {
				// enqueue secondChild in todo list
				todo[todoPos].node = secondChild;
				todo[todoPos].tmin = tPlane;
				todo[todoPos].tmax = tmax;
				++todoPos;

				node = firstChild;
				tmax = tPlane;
			}
		} else {
			// check for intersections inside leaf node
			int primitivesCount = node->getNumberPrimitives();

			if (primitivesCount == 1) {
				Intersection closetIntersection;
				float closetT;

				// check one primitive inside leaf node
				if (mPrimitives[node->mOnePrimitive]->intersect(ray, closetT, closetIntersection)) {
					if (tHit > closetT) {
						hit = true;
						tHit = closetT;
						intersection.setPrimitive(closetIntersection.getPrimitive());
						intersection.setLocalGeo(closetIntersection.getLocalGeo());
					}
				}
			} else {
				int* primIndexes = node->mPrimIndexes;
				Intersection closetIntersection;
				float closetT;

				for (int i = 0; i < primitivesCount; i++) {
					// check one primitive inside leaf node
					if (mPrimitives[primIndexes[i]]->intersect(ray, closetT, closetIntersection)) {
						if (tHit > closetT) {
							hit = true;
							tHit = closetT;
							intersection.setPrimitive(closetIntersection.getPrimitive());
							intersection.setLocalGeo(closetIntersection.getLocalGeo());
						}
					}
				}
			}

			// grab next node to process from todo list
			if (todoPos > 0.0f) {
				--todoPos;
				node = todo[todoPos].node;
				tmin = todo[todoPos].tmin;
				tmax = todo[todoPos].tmax;
			} else {
				break;
			}
		}
	}

	return hit;
}

bool KdTree::intersectP(const Ray& ray) {
	float tmin, tmax;
	if (!mAABB.intersectP(ray, tmin, tmax)) {
		return false;
	}

	// prepare to traverse kd tree for ray
	vec3 invDir(1.0f / ray.getDirection().x, 1.0f / ray.getDirection().y, 1.0f / ray.getDirection().z);
	KdToDo todo[MAX_TODO];
	int todoPos = 0;

	// traver kd tree nodes in order for ray
	const KdNode* node = &mNodes[0];

	while (node != NULL) {
		// bail out if we found a hit closer than the current node
		if (ray.getTMax() < tmin) {
			break;
		}

		if (!node->isLeaf()) {
			// process kd tree interior node

			// compute parametric distance along ray to split plane
			int axis = node->getSplitAxis();
			float tPlane = (node->getSplitPosition() - ray.getPosition()[axis]) * invDir[axis];

			// get node children pointers for ray
			const KdNode* firstChild;
			const KdNode* secondChild;
			bool belowFirst = (ray.getPosition()[axis] < node->getSplitPosition())
				|| (ray.getPosition()[axis] == node->getSplitPosition() && ray.getDirection()[axis] >= 0);

			if (belowFirst) {
				firstChild = node + 1;
				secondChild = &mNodes[node->getSecondChildNodeIndex()];
			} else {
				firstChild = &mNodes[node->getSecondChildNodeIndex()];
				secondChild = node + 1;
			}

			// advance to next child node, possibly enqueue other child
			if (tPlane > tmax || tPlane <= 0.0f) {
				node = firstChild;
			} else if (tPlane < tmin) {
				node = secondChild;
			} else {
				// enqueue secondChild in todo list
				todo[todoPos].node = secondChild;
				todo[todoPos].tmin = tPlane;
				todo[todoPos].tmax = tmax;
				++todoPos;

				node = firstChild;
				tmax = tPlane;
			}
		} else {
			// check for intersections inside leaf node
			int primitivesCount = node->getNumberPrimitives();

			if (primitivesCount == 1) {
				Primitive* prim = mPrimitives[node->mOnePrimitive];
				// check one primitive inside leaf node
				if (prim->intersectP(ray)) {
					return true;
				}
			} else {
				int* primIndexex = node->mPrimIndexes;
				for (int i = 0; i < primitivesCount; i++) {
					Primitive* prim = mPrimitives[primIndexex[i]];
					// check one primitive inside leaf node
					if (prim->intersectP(ray)) {
						return true;
					}
				}
			}

			// grab next node to process from todo list
			if (todoPos > 0.0f) {
				--todoPos;
				node = todo[todoPos].node;
				tmin = todo[todoPos].tmin;
				tmax = todo[todoPos].tmax;
			} else {
				break;
			}
		}
	}

	return false;
}
