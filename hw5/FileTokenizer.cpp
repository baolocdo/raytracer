#include "FileTokenizer.h"
#include "Utils.h"

#include <istream>
#include <iterator>
#include <sstream>

FileTokenizer::FileTokenizer(string file)
	: mFile(file)
{
}

FileTokenizer::~FileTokenizer() {
}

bool FileTokenizer::readAll(vector<vector<string>>& commands) {
	ifstream input;
	string line;

	commands.clear();

	input.open(mFile);
	if (input.is_open()) {
		getline(input, line);
		while (input) {
			line.erase(0, line.find_first_not_of(' '));
			int firstCharPosition = line.find_first_not_of(" \t\r\n");
			if (firstCharPosition != string::npos && line[firstCharPosition] != '#') {
				commands.push_back(tokenize(line));
			}
			getline(input, line);
		}
		return true;
	} else {
		Utils::error("File does not exits: " + mFile);
		return false;
	}

}

vector<string> FileTokenizer::tokenize(string str) {
	vector<string> tokens;
	istringstream iss(str);
	copy(istream_iterator<string>(iss),
		istream_iterator<string>(),
		back_inserter<vector<string> >(tokens));
	return tokens;
}