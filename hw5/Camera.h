#pragma once

#include "stdafx.h"
#include "Transformation.h"

class Camera
{
public:
	Camera(vec3 position, vec3 lookAt, vec3 up, float fovy, int width, int height);
	~Camera(void);

	vec3 getPosition() const;
	void setPosition(vec3 lookFrom);

	vec3 getLookAt() const;
	void setLookAt(vec3 lookAt);

	vec3 getUp() const;
	void setUp(vec3 up);

	float getFovY() const;
	void setFovY(float fov);

	Ray generateRay(float x, float y);
	Ray generateRayDOF(float x, float y, vec3 currentFocalPoint);

	void setApertureSize(int apertureSize);

	void setSamplingRate(int samplingrate);
	int getSamplingRate() const;

	bool isDepthOfField() const;
	void setDepthOfField(bool isDepthOfFile);

	vec3 findCurrentFocalPoint(float x, float y, Ray& primaryRay);

	void setDOFConstant();

	void setFocalLength(int focalLength);
	int getFocalLength();

	int getApertureSize();
private:
	vec3 mPosition;
	vec3 mLookAt;
	vec3 mUp;
	vec3 mDirection;
	float mDistance;

	Transformation mCameraToWorldTransformation;

	float mFovY;
	float mTanFovX, mTanFovY;
	float mAspect;

	//For depth of field only
	int mApertureSize;
	int mFocalLength;
	bool mIsDepthOfField;
	int mSamplingRate;
	float mConstant;
};

