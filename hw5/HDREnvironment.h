#pragma once

#include "stdafx.h"
#include "Environment.h"
#include "FreeImage.h"

/* High dynamic range environments */
class HDREnvironment : public Environment
{
public:
	HDREnvironment(string mapfilename);
	~HDREnvironment();
	vec3 radiance(Ray& ray);

private:
	FIBITMAP* mHDRMap;

	int mWidth;
	int mHeight;
	int mPitch;
	int mBPP;
};


