#include "Material.h"

Material::Material(BRDF brdf)
	: mBRDF(brdf), mIsTextured(false), mTexture(NULL)
{
}


Material::~Material(void)
{
	if (mTexture)
		delete mTexture;
}

BRDF Material::getBRDF(const LocalGeo& local) const {
	return mBRDF;
}

bool Material::isTextured() const {
	return mIsTextured;
}

void Material::setIsTextured(bool isTextured) {
	mIsTextured = isTextured;
}

Texture* Material::getTexture() const {
	return mTexture;
}

void Material::setTexture(string fileName) {
	mTexture = new Texture(fileName);
}

vec3 Material::getColor(LocalGeo& local) {
	if (mIsTextured) {
		float u, v;
		local.getUVCoordinate(u, v);
		return mTexture->getColorAt(u, v);
	}

	return mBRDF.getKd();
}