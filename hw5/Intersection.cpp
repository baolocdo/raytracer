#include "Intersection.h"

Intersection::Intersection(void)
{
}


Intersection::~Intersection(void)
{
}

Intersection::Intersection(const LocalGeo& localGeo, Primitive* primitive) {
	mLocalGeo = localGeo;
	mPrimitive = primitive;
}

LocalGeo& Intersection::getLocalGeo() {
	return mLocalGeo;
}

void Intersection::setLocalGeo(const LocalGeo& localGeo) {
	mLocalGeo = localGeo;
}

Primitive* Intersection::getPrimitive() const {
	return mPrimitive;
}

void Intersection::setPrimitive(Primitive* primitive) {
	mPrimitive = primitive;
}