#include "Film.h"

Film::Film(int width, int height) {
	mImage = FreeImage_Allocate(width, height, 24);
}

Film::~Film(void)
{
}

void Film::commit(int x, int y, vec3& color) {
	RGBQUAD pixelColor;

	pixelColor.rgbRed = color.x * 255.0;
	pixelColor.rgbGreen = color.y * 255.0;
	pixelColor.rgbBlue = color.z * 255.0;

	FreeImage_SetPixelColor(mImage, x, y, &pixelColor);
}

void Film::writeImage(string file) {
	if (FreeImage_Save(FIF_BMP, mImage , file.c_str(), 0))
		cout << "Image successfully saved!" << endl;
}

void Film::displayImage(string file) {	
}