#pragma once

#include "stdafx.h"

/* Abstract base class for all environments */
class Environment
{
public:
	virtual vec3 radiance(Ray& ray) = 0;
};
