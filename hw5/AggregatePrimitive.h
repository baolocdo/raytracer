#pragma once

#include "stdafx.h"
#include "AABB.h"

class AggregatePrimitive
{
public:
	AggregatePrimitive(vector<Primitive*> primitives);
	~AggregatePrimitive();

	vector<Primitive*>& getPrimitives();

	virtual bool intersect(const Ray& ray, float& tHit, Intersection& intersection) = 0;
	virtual bool intersectP(const Ray& ray) = 0;

	AABB getAABB() const;

protected:
	vector<Primitive*> mPrimitives;
	AABB mAABB;
};

