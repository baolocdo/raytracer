#include "Cone.h"
#include "Ray.h"
#include "LocalGeo.h"

Cone::Cone(const vec3 &center, float radius, float height)
	: mRadius(radius), mRadiusSquared(radius*radius),
	mHeight(height), mHeightSquared(height*height),
	mCenter(center), mConstantE(-radius*radius/(height*height))
{
}


Cone::Cone()
{
	mCenter = vec3(0.0f,1.0f,0.0f);
	mRadius = 1.0f;
	mRadiusSquared = 1.0f;
}

bool Cone::intersect(const Ray& ray, float& tHit, LocalGeo& local) {
	vec3 rayDirection = ray.getDirection();
	vec3 rayPosition = ray.getPosition();

	vec3 centerToRayOrigin = mCenter - rayPosition;
	centerToRayOrigin.y = mCenter.y - rayPosition.y + mHeight;
	vec3 intersection;

	float a = rayDirection.x * rayDirection.x
		+ mConstantE * rayDirection.y * rayDirection.y
		+ rayDirection.z * rayDirection.z;
	float b = centerToRayOrigin.x*rayDirection.x + 
		mConstantE*centerToRayOrigin.y*rayDirection.y + 
		centerToRayOrigin.z*rayDirection.z;
	float c = centerToRayOrigin.x*centerToRayOrigin.x + 
		mConstantE*centerToRayOrigin.y*centerToRayOrigin.y + 
		centerToRayOrigin.z*centerToRayOrigin.z;
	float disc = b*b - a*c;

	tHit = INFINITY;
	float oldroot = tHit;
	float d, root1, root2;
	int returnValue = 0;

	// If the discriminant is less than 0, then we totally miss the cone.

	if (disc >= 0.0f)
	{
		d = sqrt(disc);
		root2 = (b + d)/a;
		root1 = (b - d)/a;

		if (root2 < root1) {
			float temp = root1;
			root1 = root2;
			root2 = temp;
		}

		// If root2 < 0, then root1 is also < 0, so they are both misses.
		if (root2 >= 0.0f) {
			// If root2 > 0, and root1 < 0, we are inside the cone.
			if (root1 < 0) {
				if (root2 < tHit) { 
					tHit = root2; 
					returnValue = -1; 
				}
				// If root2 > 0, and root1 > 0, we are hit the cone.
			} else {
				if (root1 < tHit) { 
					tHit = root1; 
					returnValue = 1; 
				}
			}
		} else {
			return false;
		}

		intersection = rayPosition + rayDirection*tHit ;

		// Limit the y values: ymin <= y <= ymax
		// If the point of intersection is too low or too high, record it as a miss.
		// Return the old tHit value, and return a miss.
		if ((intersection.y > (mCenter.y + mHeight)) || (intersection.y < mCenter.y)) {
			intersection = rayPosition + rayDirection*root2 ;
			// Are we too high in our first hit, but hit the back wall later
			if ((intersection.y > (mCenter.y + mHeight)) || (intersection.y < mCenter.y)) {
				tHit = oldroot; 
				returnValue = 0;
			}
		} 
		// Check to see if it hits the end planes
		// We know the normal of the end planes is y_min=y_max=(0,1,0)
		// Therfore the dot product is simply the y portion of the
		// We know the point Q = y_min or y_max
		// If the direction of the Ray is not parallel to the top or bottom...
		if ((rayDirection.y != 0.0f) && (returnValue <= 0)) {
			// check bottom cap
			root1 = -( rayPosition.y - mCenter.y)/rayDirection.y;
			if (root1 > 0.0f) {
				intersection = rayPosition + rayDirection*root1 - mCenter;
				if (intersection.x*intersection.x + intersection.z*intersection.z <= mRadiusSquared) {
					if (root1 < oldroot) {
						tHit = root1; 
						returnValue = 2; 
					}
				}
			} else {
				return false;
			}
		} // end if rayDirection.y

		if (returnValue == 0 || tHit <= ray.getTMin() || tHit >= ray.getTMax())
			return false;

		//intersection = ray.getPosition() + tHit * ray.getDirection();

		local.setPosition(intersection);
		local.setNormal(getNormal(intersection));

		return true;
	}

	return false;
}

bool Cone::intersectP(const Ray& ray) {
	//return false;
	vec3 rayDirection = ray.getDirection();
	vec3 rayPosition = ray.getPosition();

	vec3 centerToRayOrigin = mCenter - rayPosition;
	centerToRayOrigin.y = mCenter.y - rayPosition.y + mHeight;
	vec3 intersection;

	float a = rayDirection.x * rayDirection.x
		+ mConstantE * rayDirection.y * rayDirection.y
		+ rayDirection.z * rayDirection.z;
	float b = centerToRayOrigin.x*rayDirection.x + 
		mConstantE*centerToRayOrigin.y*rayDirection.y + 
		centerToRayOrigin.z*rayDirection.z;
	float c = centerToRayOrigin.x*centerToRayOrigin.x + 
		mConstantE*centerToRayOrigin.y*centerToRayOrigin.y + 
		centerToRayOrigin.z*centerToRayOrigin.z;
	float disc = b*b - a*c;

	float tHit = INFINITY;
	float oldroot = tHit;
	float d, root1, root2;
	int returnValue = 0;

	// If the discriminant is less than 0, then we totally miss the cone.

	if (disc >= 0.0f)
	{
		d = sqrt(disc);
		root2 = (b + d)/a;
		root1 = (b - d)/a;

		if (root2 < root1) {
			float temp = root1;
			root1 = root2;
			root2 = temp;
		}

		// If root2 < 0, then root1 is also < 0, so they are both misses.
		if (root2 >= 0.0f) {
			// If root2 > 0, and root1 < 0, we are inside the cone.
			if (root1 < 0) {
				if (root2 < tHit) { 
					tHit = root2; 
					returnValue = -1; 
				}
				// If root2 > 0, and root1 > 0, we are hit the cone.
			} else {
				if (root1 < tHit) { 
					tHit = root1; 
					returnValue = 1; 
				}
			}
		} else {
			return false;
		}

		intersection = rayPosition + rayDirection*tHit ;

		// Limit the y values: ymin <= y <= ymax
		// If the point of intersection is too low or too high, record it as a miss.
		// Return the old tHit value, and return a miss.
		if ((intersection.y > (mCenter.y + mHeight)) || (intersection.y < mCenter.y)) {
			intersection = rayPosition + rayDirection*root2 ;
			// Are we too high in our first hit, but hit the back wall later
			if ((intersection.y > (mCenter.y + mHeight)) || (intersection.y < mCenter.y)) {
				tHit = oldroot; 
				returnValue = 0;
			}
		} 
		// Check to see if it hits the end planes
		// We know the normal of the end planes is y_min=y_max=(0,1,0)
		// Therfore the dot product is simply the y portion of the
		// We know the point Q = y_min or y_max
		// If the direction of the Ray is not parallel to the top or bottom...
		if ((rayDirection.y != 0.0f) && (returnValue <= 0)) {
			// check bottom cap
			root1 = -( rayPosition.y - mCenter.y)/rayDirection.y;
			if (root1 > 0.0f) {
				intersection = rayPosition + rayDirection*root1 - mCenter;
				if (intersection.x*intersection.x + intersection.z*intersection.z <= mRadiusSquared) {
					if (root1 < oldroot) {
						tHit = root1; 
						returnValue = 2; 
					}
				}
			} else {
				return false;
			}
		} // end if rayDirection.y

		if (returnValue == 0 || tHit < ray.getTMin() || tHit > ray.getTMax())
			return false;

		return true;
	}

	return false;
}

bool Cone::intersectP(const AABB& box) {
	return false;
}

void Cone::findUV(vec3 position, float& x, float& y) {

}

AABB Cone::computeAABB() const {
	return AABB();
}

vec3 Cone::getNormal(vec3 &intersection)
{
	float a, b, c;

	if (intersection.y == mCenter.y) { 
		return vec3 (0.0f, -1.0f, 0.0f); 
	}

	a = intersection.x - mCenter.x;
	b = intersection.y - mCenter.y - mHeight;
	c = intersection.z - mCenter.z;

	return glm::normalize(vec3(a, mConstantE*b,c));
}
