#include "KdNode.h"

void KdNode::initLeaf(int* primIndexes, int primitivesCount) {
	mFlags = 3;
	mPrimitivesCount |= (primitivesCount << 2);
	// store primIndexes ids for leaf node
	if (primitivesCount == 0) {
		mOnePrimitive = 0;
	} else  if (primitivesCount == 1) {
		mOnePrimitive = primIndexes[0];
	} else {
		//primIndexes = arena.Alloc<int>(primitivesCount);
		mPrimIndexes = new int[primitivesCount];
		for (int i = 0; i < primitivesCount; i++) {
			mPrimIndexes[i] = primIndexes[i];
		}
	}
}

void KdNode::initInterior(int axis, int aboveChildIndex, float splitPosition) {
	mSplit = splitPosition;
	mFlags = axis;
	mAboveChild |= (aboveChildIndex << 2);
}

float KdNode::getSplitPosition() const {
	return mSplit;
}

int KdNode::getNumberPrimitives() const {
	return mPrimitivesCount >> 2;
}

int KdNode::getSplitAxis() const {
	return mFlags & 3;
}

bool KdNode::isLeaf() const {
	return (mFlags & 3) == 3;
}

int KdNode::getSecondChildNodeIndex() const {
	return mAboveChild >> 2;
}