#include "Texture.h"

Texture::Texture(void)
{
	mImage = NULL;
}

Texture::~Texture(void)
{
	if (mImage)
		delete mImage;
}

Texture::Texture(string fileName) {
	mFileName = fileName;
	mImage = FreeImage_Load(FIF_PNG, fileName.c_str(), PNG_DEFAULT);
}

void Texture::setTexture(string fileName) {
	mFileName = fileName;
	mImage = FreeImage_Load(FIF_PNG, fileName.c_str(), PNG_DEFAULT);
}

vec3 Texture::getColorAt(float u, float v) {
	RGBQUAD* pixelColor = new RGBQUAD();
	int height = FreeImage_GetWidth(mImage);
	int width = FreeImage_GetHeight(mImage);
	unsigned int x = (int) ((u/2.0f + 0.5f)* width);
	unsigned int y = (int) ((v/2.0f + 0.5f)* height);

	FreeImage_GetPixelColor(mImage, x, height - y, pixelColor);
	return vec3((float)pixelColor->rgbRed/255, (float)pixelColor->rgbGreen/255, (float)pixelColor->rgbBlue/255);
}