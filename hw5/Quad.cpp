#include "Quad.h"
#include "Triangle.h"

Quad::Quad(void)
{
	mMinPosition = vec3(0.0f);
	mMaxPosition = vec3(1.0f, 1.0f, 0.0f);

	vec3 lowerRight(mMaxPosition.x, mMinPosition.y, 0.0f);
	vec3 upperLeft(mMinPosition.x, mMaxPosition.y, 0.0f);

	mTri1 = new Triangle(mMaxPosition, mMinPosition, lowerRight);
	mTri2 = new Triangle(mMinPosition, mMaxPosition, upperLeft);
}


Quad::~Quad(void)
{
	if (mTri1)
		delete mTri1;

	if (mTri2)
		delete mTri2;
}


Quad::Quad(vec3 minPosition, vec3 maxPosition) 
	: mMinPosition(minPosition), mMaxPosition(maxPosition)
{
	vec3 lowerRight(mMaxPosition.x, mMinPosition.y, mMaxPosition.z);
	vec3 upperLeft(mMinPosition.x, mMaxPosition.y, mMinPosition.z);

	mTri1 = new Triangle(mMaxPosition, mMinPosition, lowerRight);
	mTri2 = new Triangle(mMinPosition, mMaxPosition, upperLeft);
}

bool Quad::intersect(const Ray& rRay, float& tHit, LocalGeo& local) {
	return mTri1->intersect(rRay, tHit, local) || mTri2->intersect(rRay, tHit, local);
}

bool Quad::intersectP(const Ray& ray) {
	return mTri1->intersectP(ray) || mTri2->intersectP(ray);
}

bool Quad::intersectP(const AABB& box) {
	return false;
}

void Quad::findUV(vec3 position, float& x, float& y) {

}

AABB Quad::computeAABB() const {
	return AABB();
}