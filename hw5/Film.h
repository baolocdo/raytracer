#pragma once

#include "stdafx.h"
#include "FreeImage.h"

class Film
{
public:
	Film(int width, int height);
	~Film(void);

	void commit(int x, int y, vec3& color);
	void writeImage(string file);

	void displayImage(string fileName);

private:
	FIBITMAP* mImage;
};

