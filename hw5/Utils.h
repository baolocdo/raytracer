#pragma once

#include "stdafx.h"

class Utils
{
public:
	static void pauseConsole();
	static void log(string msg);
	static void error(string msg);
	static void fatalError(string msg);

	static long getTime();
};

