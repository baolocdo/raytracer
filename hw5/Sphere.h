#pragma once

#include "stdafx.h"
#include "Shape.h"

class Sphere
	: public Shape
{
public:
	Sphere(vec3 center, float radius);
	~Sphere(void);

	vec3 getCenter() const;
	void setCenter(vec3 center);

	float getRadius() const;
	void setRadius(float radius);

	virtual bool intersect(const Ray& ray, float& tHit, LocalGeo& local);
	virtual bool intersectP(const Ray& ray);
	virtual bool intersectP(const AABB& box);

	void findUV(vec3 currentPosition, float &u, float &v);
	virtual AABB computeAABB() const;

private:
	vec3 mCenter;
	float mRadius;
};

