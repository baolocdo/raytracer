#include "Scene.h"
#include "Material.h"
#include "Utils.h"
#include "FileTokenizer.h"
#include "Transform.h"
#include "SuperSampler.h"
#include "GeometricPrimitive.h"
#include "Sphere.h"
#include "Triangle.h"
#include "PointLight.h"
#include "DirectionalLight.h"
#include "SphereLight.h"
#include "AreaLight.h"
#include "ConstantEnvironment.h"
#include "HDREnvironment.h"
#include "Cylinder.h"
#include "Box.h"
#include "Cone.h"
#include "Quad.h"
#include "Film.h"

#include "KdTree.h"
#include "NaiveAggregatePrimitive.h"

#include "Tracer.h"
#include "PathTracer.h"
#include "RayTracer.h"

#include <omp.h>
#include <stack>

#define READ_INT(x) atoi(command[x].c_str())
#define READ_FLOAT(x) atof(command[x].c_str())
#define READ_VEC3(x) vec3(READ_FLOAT(x), READ_FLOAT(x + 1), READ_FLOAT(x + 2))

#define DEFAULT_BACKGROUND_COLOR vec3(0.0f)
#define DEFAULT_AMBIENT vec3(0.2f)
#define DEFAULT_ATTENUATION vec3(1.0f, 0.0f, 0.0f)

#define USE_KD_TREE 0

#define BLOCK_SIZE 64

Scene::Scene(string fileName) {
	Scene(fileName, "output1.png");
}

Scene::Scene(string fileName, string outputFile)
	: mCamera(NULL)
	, mSampler(NULL)
	, mTracer(NULL)
	, mFilm(NULL)
	, mLightPrimitives(NULL)
	, mEnvironment(NULL)
	, mOutputFileName(outputFile)
	, mSamplingRate(1)
{
	loadScene(fileName);
}

Scene::~Scene(void)
{
	cleanUp();
}

void Scene::cleanUp() {
	if (mCamera) {
		delete mCamera;
	}
	if (mSampler) {
		delete mSampler;
	}
	if (mTracer) {
		delete mTracer;
	}
	if (mFilm) {
		delete mFilm;
	}
	if (mEnvironment) {
		delete mEnvironment;
	}
	for (vector<Light*>::iterator it = mLights.begin(); it != mLights.end(); ++it) {
		delete *it;
	}
	for (vector<Primitive*>::iterator it = mPrimitives.begin(); it != mPrimitives.end(); ++it) {
		delete *it;
	}
	for (vector<Primitive*>::iterator it = mLightPrimitives.begin(); it != mLightPrimitives.end(); ++it) {
		delete *it;
	}
}

void Scene::reset() {
	cleanUp();

	mEnvironment = new ConstantEnvironment(DEFAULT_BACKGROUND_COLOR);

	mVectices.clear();
	mNormals.clear();
	mNormalVertices.clear();

	mWidth = 200;
	mHeight = 200;
	mMaxDepth = 5;
}

void Scene::loadScene(string file) {
	vector<vector<string>> lines;
	if (!FileTokenizer(file.c_str()).readAll(lines)) {
		return;
	}

	reset();

	BRDF brdf;
	stack<mat4> transformMatrices;
	mat4 currentTransformMatrix(1.0f);

	brdf.setKa(DEFAULT_AMBIENT);
	vec3 attenuation(DEFAULT_ATTENUATION);

	RenderMode renderMode = RAYTRACER;
	//SamplingMode samplingMode = NORMAL;

	//Time interval
	float startTime = 0.0f;
	float endTime = 0.0f;
	
	// Sample rays for path tracer. Default is 32
	int numberOfSampleRays = 32;

	for (vector<vector<string>>::iterator it = lines.begin() ; it < lines.end(); ++it) {
		vector<string> command = *it;
		if (command[0].compare("size") == 0 && command.size() == 3) {
			this->mWidth = READ_INT(1);
			this->mHeight = READ_INT(2);

		} else if (command[0].compare("camera") == 0 && command.size() == 11) {
			mCamera = new Camera(READ_VEC3(1), READ_VEC3(4), READ_VEC3(7), READ_FLOAT(10), mWidth, mHeight);

		} else if (command[0].compare("maxdepth") == 0 && command.size() == 2) {
			this->mMaxDepth = READ_INT(1);

		} else if (command[0].compare("sphere") == 0 && command.size() == 5) {
			Sphere* sphere = new Sphere(READ_VEC3(1), READ_FLOAT(4));
			Material* material = new Material(brdf);
			mPrimitives.push_back(new GeometricPrimitive(sphere, material, Transformation(currentTransformMatrix)));

		} else if (command[0].compare("animatedSphere") == 0 && command.size() == 8) {
			Sphere* sphere = new Sphere(READ_VEC3(1), READ_FLOAT(4));
			Material* material = new Material(brdf);
			mPrimitives.push_back(new GeometricPrimitive(sphere, material, Transformation(currentTransformMatrix), READ_VEC3(5)));

		} else if (command[0].compare("output") == 0 && command.size() == 2) {
			this->mOutputFileName = command[1].c_str();

		} else if (command[0].compare("maxverts") == 0 && command.size() == 2) {
			// No need

		} else if (command[0].compare("maxvertnorms") == 0 && command.size() == 2) {
			// No need

		} else if (command[0].compare("vertex") == 0 && command.size() == 4) {
			mVectices.push_back(READ_VEC3(1));

		} else if (command[0].compare("vertexnormal") == 0 && command.size() == 7) {
			// TODO: implement
			mNormalVertices.push_back(READ_VEC3(1));
			mNormals.push_back(READ_VEC3(4));

		} else if (command[0].compare("tri") == 0 && command.size() == 4) {
			Triangle* triangle = new Triangle(mVectices[READ_INT(1)], mVectices[READ_INT(2)], mVectices[READ_INT(3)]);
			Material* material = new Material(brdf);
			mPrimitives.push_back(new GeometricPrimitive(triangle,	material, Transformation(currentTransformMatrix)));

		} else if (command[0].compare("trinormal") == 0 && command.size() == 4) {
			Triangle* triangle = new Triangle(mNormalVertices[READ_INT(1)], mNormalVertices[READ_INT(2)], mNormalVertices[READ_INT(3)], 
				mNormals[READ_INT(1)], mNormals[READ_INT(2)], mNormals[READ_INT(3)]);
			Material* material = new Material(brdf);
			mPrimitives.push_back(new GeometricPrimitive(triangle,	material, Transformation(currentTransformMatrix)));

		} else if (command[0].compare("translate") == 0 && command.size() == 4) {
			currentTransformMatrix = currentTransformMatrix * glm::transpose(Transform::translate(READ_FLOAT(1), READ_FLOAT(2), READ_FLOAT(3)));

		} else if (command[0].compare("rotate") == 0 && command.size() == 5) {
			currentTransformMatrix = currentTransformMatrix * glm::transpose(Transform::rotate(READ_VEC3(1), glm::radians(READ_FLOAT(4))));

		} else if (command[0].compare("scale") == 0 && command.size() == 4) {
			currentTransformMatrix = currentTransformMatrix * glm::transpose(Transform::scale(READ_FLOAT(1), READ_FLOAT(2), READ_FLOAT(3)));

		} else if (command[0].compare("pushTransform") == 0 && command.size() == 1) {
			transformMatrices.push(currentTransformMatrix);

		} else if (command[0].compare("popTransform") == 0 && command.size() == 1) {
			currentTransformMatrix = transformMatrices.top();
			transformMatrices.pop();

		} else if (command[0].compare("directional") == 0 && command.size() == 7) {
			DirectionalLight* newLight = new DirectionalLight();
			newLight->setDirection(READ_VEC3(1));
			newLight->setColor(READ_VEC3(4));

			mLights.push_back(newLight);

		} else if (command[0].compare("point") == 0 && command.size() == 7) {
			PointLight* newLight = new PointLight(attenuation);
			newLight->setPosition(READ_VEC3(1));
			newLight->setColor(READ_VEC3(4));

			mLights.push_back(newLight);

		} else if (command[0].compare("attenuation") == 0 && command.size() == 4) {
			attenuation = READ_VEC3(1);		

		} else if (command[0].compare("ambient") == 0 && command.size() == 4) {
			brdf.setKa(READ_VEC3(1));

		} else if (command[0].compare("diffuse") == 0 && command.size() == 4) {
			brdf.setKd(READ_VEC3(1));

		} else if (command[0].compare("specular") == 0 && command.size() == 4) {
			brdf.setKs(READ_VEC3(1));

		} else if (command[0].compare("shininess") == 0 && command.size() == 2) {
			brdf.setShininess(READ_FLOAT(1));

		} else if (command[0].compare("emission") == 0 && command.size() == 4) {
			brdf.setKe(READ_VEC3(1));

		} else if (command[0].compare("reflection") == 0 && command.size() == 2) {
			brdf.setKr(READ_FLOAT(1));

		} else if (command[0].compare("refraction") == 0 && command.size() == 2) {
			brdf.setKt(READ_FLOAT(1));

		} else if (command[0].compare("refractiveIndex") == 0 && command.size() == 2) {
			brdf.setRefractiveIndex(READ_FLOAT(1));

		} else if (command[0].compare("samplingRate") == 0 && command.size() == 2) {
			mSamplingRate = READ_INT(1);

		} else if (command[0].compare("sphereLight") == 0 && command.size() == 9) {
			mLights.push_back(new SphereLight(READ_VEC3(1), READ_INT(4), READ_VEC3(5), READ_INT(8)));

			BRDF newBrdf;
			newBrdf.setKe(READ_VEC3(5));
			newBrdf.setKa(vec3(0.0f));
			newBrdf.setKr(0.0f);
			Sphere* sphere = new Sphere(READ_VEC3(1), READ_FLOAT(4));
			Material* material = new Material(newBrdf);
			mLightPrimitives.push_back(new GeometricPrimitive(sphere, material, Transformation(currentTransformMatrix)));

		} else if (command[0].compare("focalLength") == 0 && command.size() == 2) {
			mCamera->setFocalLength(READ_INT(1));

		} else if (command[0].compare("apertureSize") == 0 && command.size() == 2) {
			mCamera->setApertureSize(READ_INT(1));

		} else if (command[0].compare("isDepthOfField") == 0 && command.size() == 2) {
			if (command[1].compare("true") == 0)
				mCamera->setDepthOfField(true);
			else
				mCamera->setDepthOfField(false);

		} else if (command[0].compare("DOFSamplingRate") == 0 && command.size() == 2) {
			mCamera->setSamplingRate(READ_INT(1));

		} else if (command[0].compare("isGlossy") == 0 && command.size() == 2) {
			if (command[1].compare("true") == 0)
				brdf.setIsGlossy(true);
			else
				brdf.setIsGlossy(false);
		} else if (command[0].compare("glossySamplingRate") == 0 && command.size() == 2) {
			brdf.setGlossySamplingRate(READ_INT(1));

		} else if (command[0].compare("glossyDegreeOfBlur") == 0 && command.size() == 2) {
			brdf.setGlossyDegreeOfBlur(READ_FLOAT(1));

		} else if (command[0].compare("texturedSphere") == 0 && command.size() == 6) {
			Sphere* sphere = new Sphere(READ_VEC3(1), READ_FLOAT(4));
			Material* material = new Material(brdf);
			material->setIsTextured(true);
			material->setTexture(command[5]);
			mPrimitives.push_back(new GeometricPrimitive(sphere, material, Transformation(currentTransformMatrix)));

		} else if (command[0].compare("renderMode") == 0 && command.size() == 3) {
			if (command[1].compare("pathtracing") == 0) {
				renderMode = PATHTRACER;
				numberOfSampleRays = READ_INT(2);

			} else {
				renderMode = RAYTRACER;
			}

			//AreaLight position(3), vector edge 1(3), vector edge(3), intensity(3), samplingRate(1)
		} else if (command[0].compare("areaLight") == 0 && command.size() == 14) {
			mLights.push_back(new AreaLight(READ_VEC3(1), READ_VEC3(4), READ_VEC3(7), READ_VEC3(10), attenuation, READ_INT(13)));

		} else if (command[0].compare("HDREnvironmentMap") == 0 && command.size() == 2) {
			HDREnvironment* HDRMap = new HDREnvironment(command[1]);
			mEnvironment = HDRMap;

			//Cylinder: center position(3), radius(1), top position(3), bottom position(3), 
		} else if (command[0].compare("cylinder") == 0 && command.size() == 11) {
			Cylinder* cylinder = new Cylinder(READ_VEC3(1), READ_INT(4), READ_VEC3(5), READ_VEC3(8));
			Material* material = new Material(brdf);
			mPrimitives.push_back(new GeometricPrimitive(cylinder, material, Transformation(currentTransformMatrix)));

			//Box: min position(3), max position(3)
		} else if (command[0].compare("box") == 0 && command.size() == 7) {
			Box* box = new Box(READ_VEC3(1), READ_VEC3(4));
			Material* material = new Material(brdf);
			mPrimitives.push_back(new GeometricPrimitive(box, material, Transformation(currentTransformMatrix)));

			//Cone: center(3), radius(1), height(1)
		} else if (command[0].compare("cone") == 0 && command.size() == 6) {
			Cone* cone = new Cone(READ_VEC3(1), READ_FLOAT(4), READ_FLOAT(5));
			Material* material = new Material(brdf);
			mPrimitives.push_back(new GeometricPrimitive(cone, material, Transformation(currentTransformMatrix)));

			//Quad: min position(3) max position (3)
		} else if (command[0].compare("quad") == 0 && command.size() == 7) {
			Quad* quad = new Quad(READ_VEC3(1), READ_VEC3(4));
			Material* material = new Material(brdf);
			mPrimitives.push_back(new GeometricPrimitive(quad, material, Transformation(currentTransformMatrix)));

		} else if (command[0].compare("motionBlur") == 0 && command.size() == 3) {
			/*if (command[1].compare("antialiasing") && command.size() == 3) {
				mSamplingRate = READ_INT(2);
				samplingMode = ANTIALIASING;

			} else if (command[1].compare("depthOfField") && command.size() ==  5) {
				samplingMode = DEPTH_OF_FIELD;
				mCamera->setFocalLength(READ_INT(1));
				mCamera->setApertureSize(READ_INT(2));
				mCamera->setSamplingRate(READ_INT(3));

			} else if (command[1].compare("motionBlur") && command.size() == 4)*/ {
				//samplingMode = MOTION_BLUR;
				startTime = READ_FLOAT(1);
				endTime = READ_FLOAT(2);
			}
		} else {
			Utils::error("Invalid command in scene file: " + command[0]);
		}
	}

#if USE_KD_TREE
	if (renderMode == RAYTRACER) {
		mTracer = new RayTracer(new KdTree(mPrimitives, 80, 2, 0.7f, 3, -1),
			mLights, mMaxDepth, mEnvironment);
	} else {
		KdTree* kdTree = new KdTree(mPrimitives, 80, 2, 0.7f, 3, -1);
		mTracer = new PathTracer(kdTree, mLights, mMaxDepth, mEnvironment, numberOfSampleRays);
	}
#else	
	if (renderMode == RAYTRACER) {
		mTracer = new RayTracer(new NaiveAggregatePrimitive(mPrimitives),
			mLights, mMaxDepth, mEnvironment);
	} else {
		mTracer = new PathTracer(new NaiveAggregatePrimitive(mPrimitives),
			mLights, mMaxDepth, mEnvironment, numberOfSampleRays);
	}
#endif

	mSampler = new SuperSampler(mWidth, mHeight, mSamplingRate, startTime, endTime);
	mFilm = new Film(mWidth, mHeight);
}

void Scene::renderToFilm() {
	float start = (float) Utils::getTime();

    #pragma omp parallel for
	for (int i = 0; i < mWidth; i++) {
		for (int j = 0; j < mHeight; j++) {
			mFilm->commit(i, j, mSampler->sample(i, j, mTracer, mCamera));
		}
	}

	float end = ((float) Utils::getTime()) - start;
	cout << "Render finished in: " << end << " microseconds." << endl;
}

void Scene::saveToFile() {
	renderToFilm();
	mFilm->writeImage(mOutputFileName);
}

Camera* Scene::getCamera() const {
	return mCamera;
}

Sampler* Scene::getSampler() const {
	return mSampler;
}

Tracer* Scene::getTracer() const {
	return mTracer;
}

Film* Scene::getFilm() const {
	return mFilm;
}

vector<Light*> Scene::getLights() const {
	return mLights;
}

int Scene::getWidth() const {
	return mWidth;
}

void Scene::setWidth(int width) {
	mWidth = width;
}

int Scene::getHeight() const {
	return mHeight;
}

void Scene::setHeight(int height) {
	mHeight = height;
}

int Scene::getMaxDepth() const {
	return mMaxDepth;
}

void Scene::setMaxDepth(int maxDepth) {
	mMaxDepth = maxDepth;
}

int Scene::getSamplingRate() const {
	return mSamplingRate;
}

void Scene::setSamplingRate(int samplingRate) {
	mSamplingRate = samplingRate;
}