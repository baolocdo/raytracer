#Cornell Box
#size 100 100
size 640 480
camera 0 0 1 0 0 -1 0 1 0 45
output scene10.bmp
renderMode pathtracing 200

maxdepth 4
attenuation 1 0.1 0.05

#emission 0.9 0.9 0.9
#point 0 0.45 -1.5 0.9 0.9 0.9
#sphereLight 0 0.55 -1.5 0.1 0.9 0.9 0.9 16
areaLight -0.25 0.40 -2 0 0 0.6 0.6 0 0 0.6 0.5 0.5 5

maxverts 12
#planar face
vertex -1 +1 0
vertex -1 -1 0
vertex +1 -1 0
vertex +1 +1 0

ambient 0 0 0
specular 0 0 0
shininess 1
emission 0 0 0
diffuse 0 0 0

#point 0 0.44 -1.5 0.8 0.8 0.8
#sphereLight 0 0.44 1 -1.5 0.8 0.8 0.8 512
#directional 0 1 -1 0.2 0.2 0.2

diffuse 0 0 1
#sphere 0 0.8 -1.5 0.1

#attenuation 1 0.1 0.05

pushTransform

#red
pushTransform
translate 0 0 -3
rotate 0 1 0 60
scale 10 10 1
diffuse 1 0 0
quad -1 -1 0 1 1 0
#tri 0 1 2
#tri 0 2 3
popTransform

#green
pushTransform
translate 0 0 -3
rotate 0 1 0 -60
scale 10 10 1
diffuse 0 1 0
quad -1 -1 0 1 1 0
popTransform

#back
pushTransform
scale 10 10 1
translate 0 0 -2
diffuse 1 1 1
quad -1 -1 0 1 1 0
popTransform

#top
pushTransform
translate 0 0.5 0
rotate 1 0 0 60
translate 0 0 -1
scale 10 10 1
diffuse 1 1 1
quad -1 -1 0 1 1 0
popTransform

#bottom
pushTransform
translate 0 0.1 0
rotate 1 0 0 -90
translate 0 0 -1
scale 10 10 1
diffuse 1 1 1
quad -1 -1 0 1 1 0
popTransform

#cube
#diffuse 0.5 0.7 0.2
#specular 0.2 0.2 0.2
#pushTransform
#translate -0.25 -0.5 -1.8
#rotate 0 1 0 25
#scale 0.25 0.4 0.2
#diffuse 1 1 1

#box -1 -1 1 1 1 -1
#popTransform

#sphere
#diffuse 0.5 0.5 0.5
diffuse 0.1 0.1 0.1
specular 0.5 0.5 0.5
shininess 200
reflection 1.0
refraction 1
refractiveIndex 1.1
pushTransform
translate 0.4 -0.7 -1.5
rotate 0 1 0 -30
scale 0.2 0.2 0.2
#diffuse 1 1 1

sphere 0 0 0 1
#box -1 -1 1 1 1 -1
popTransform

#cone
#diffuse 0.2 0.2 0.2
#specular 0.2 0.2 0.2
#pushTransform
#translate -0.1 -0.9 -1.5
#rotate 0 1 0 -30
#scale 0.2 0.4 0.2
#diffuse 1 1 1

#cone 0 0 0 1.0 1.0 
#sphere 0 0 0 1
#box -1 -1 1 1 1 -1
#popTransform



popTransform
