#pragma once

#include "Environment.h"

/* Environments that are just a constant background color */
class ConstantEnvironment : public Environment
{
public:
	ConstantEnvironment(vec3 color);
	vec3 radiance(Ray& ray);

private:
	vec3 mBackgroundColor;
};
