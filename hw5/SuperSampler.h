#pragma once

#include "Sampler.h"

class SuperSampler
	: public Sampler
{
public:
	SuperSampler(int width, int height, int samplingRate, float startTime = 0.0f, float endTime = 0.0f);
	~SuperSampler(void);
	virtual vec3 sample(int x0, int y0, Tracer* rayTracer, Camera* camera);

private:	
	int mSamplingRate;
	float mStartTime, mEndTime;
	float dx, dy;
};

