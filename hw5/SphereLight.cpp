#include "SphereLight.h"
#include "Ray.h"
#include "AggregatePrimitive.h"
#include "Intersection.h"

SphereLight::SphereLight(void) {
	mRadius = 0;
	mPosition = vec3(0.0f);
	mSamplingRate = 20;
	mAttenuation = vec3(1.0f, 0.0f, 0.0f);
}

SphereLight::~SphereLight(void)
{
}

SphereLight::SphereLight(vec3 position, int radius, vec3 color, int samplingRate)
	: mPosition(position), mRadius(radius), mSamplingRate(samplingRate) {
		mAttenuation = vec3(1.0f, 0.0f, 0.0f);
		mColor = color;
}

void SphereLight::generateLightRay(LocalGeo& local, Ray& lightRay) {
	vec3 newPointOnSphere = generateRandomPoint();

	lightRay.setDirection(glm::normalize(newPointOnSphere - local.getPosition()));
	lightRay.setPosition(local.getPosition());

	lightRay.setTMax(glm::length(newPointOnSphere - local.getPosition()));
}

vec3 SphereLight::shading(LocalGeo& local, Material* material, Ray& lightRay, vec3 eyeDir) {
	float distance = glm::length(mPosition - local.getPosition());
	float attenuation = (mAttenuation.x + mAttenuation.y * distance + mAttenuation.z * distance * distance);
	vec3 returnColor = computeLight(local, material, lightRay, eyeDir);
	return returnColor / attenuation;
}

vec3 SphereLight::lightColor(LocalGeo& local, Material* material, AggregatePrimitive* primitives, Ray& ray) {
	Ray lightRay;
	vec3 color(0.0f);

	for (int i = 0; i < mSamplingRate; i++) {
		generateLightRay(local, lightRay);

		if (glm::dot(local.getNormal(), lightRay.getDirection()) > 0.0f) {
			if (!primitives->intersectP(lightRay)) {
				color += shading(local, material, lightRay, ray.getDirection());
			}
		}
	}

	return vec3(color.x / mSamplingRate, color.y / mSamplingRate, color.z / mSamplingRate);
}

vec3 SphereLight::generateRandomPoint() {
	float u = rand() / (float)RAND_MAX;
	float v = rand() / (float)RAND_MAX;

	float q = 2 * PI * u;
	float f = glm::acos(2 * v - 1);

	vec3 newPointOnSphere;
	newPointOnSphere.x = mRadius * glm::cos(q)*glm::sin(f);
	newPointOnSphere.y = mRadius * glm::sin(q)*glm::sin(f);
	newPointOnSphere.z = mRadius * glm::cos(f);

	return newPointOnSphere + mPosition;
}

