#pragma once

#include "Light.h"

class SphereLight
	: public Light
{
public:
	SphereLight();
	SphereLight(vec3 position, int radius, vec3 color, int samplingRate);
	~SphereLight(void);

	virtual void generateLightRay(LocalGeo& local, Ray& lightRay);
	virtual vec3 shading(LocalGeo& local, Material* material, Ray& lightRay, vec3 eyeDir);

	virtual vec3 lightColor(LocalGeo& local, Material* material, AggregatePrimitive* primitives, Ray& ray);

	vec3 generateRandomPoint();

private:
	int mSamplingRate;
	vec3 mPosition;
	int mRadius;
	vec3 mAttenuation;
};

