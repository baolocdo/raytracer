#include "DirectionalLight.h"
#include "Ray.h"
#include "AggregatePrimitive.h"
#include "LocalGeo.h"

DirectionalLight::DirectionalLight(void)
{
}

DirectionalLight::~DirectionalLight(void)
{
}

void DirectionalLight::generateLightRay(LocalGeo& local, Ray& lightRay) {
	lightRay.setDirection(mDirection);
	lightRay.setPosition(local.getPosition());
}

vec3 DirectionalLight::getDirection() const {
	return mDirection;
}

void DirectionalLight::setDirection(vec3 direction) {
	mDirection = glm::normalize(direction);
}

vec3 DirectionalLight::shading(LocalGeo& local, Material* material, Ray& lightRay, vec3 eyeDir) {
	return computeLight(local,material, lightRay, eyeDir);
}

vec3 DirectionalLight::lightColor(LocalGeo& local, Material* material, AggregatePrimitive* primitives, Ray& ray) {
	Ray lightRay;
	generateLightRay(local, lightRay);

	if (glm::dot(local.getNormal(), lightRay.getDirection()) > 0.0f) {
		if (!primitives->intersectP(lightRay)) {
			return shading(local, material, lightRay, ray.getDirection());
		}

	}

	return vec3(0.0f);
}