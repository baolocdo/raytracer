#pragma once

#include "stdafx.h"
#include "Tracer.h"

class RayTracer
	: public Tracer
{
public:
	RayTracer(AggregatePrimitive* primitives, vector<Light*> lights, int maxDepth, Environment* evironment);
	~RayTracer(void);

	vec3 trace(Ray& ray, int depth);

private:
	Ray createReflectRay(LocalGeo& local, Ray& ray);
	bool createRefractionRay(vec3 normal, vec3 hitPosition, Ray& incomingRay, float reflectiveIndex1, float reflectiveIndex2, Ray& refractionRay);
	Light* intersectWithLights(Ray& ray, float& tHit, Intersection& intersection);

	//Refraction
	vec3 dieletricRefract(Ray& ray, Intersection& intersection, int depth, vec3 reflColor);
	bool dielectricCalc(Ray& ray, Intersection& intersection, Ray& refractRay, float& schlick);
	bool createRefractedRay(Ray& ray, Intersection& intersection, float oldIndex, float newIndex, Ray& refractRay);
	float schlickCalc(float index1, float index2, vec3& rayDirection, vec3& refractDirection, vec3& normal);

	//For glossy 
	vec3 findGlossyColor(BRDF& brdf, Ray& reflectRay, int depth);
	Ray createRandomReflectRay(Ray& reflectRay, float degreeOfBlur);
};

