#pragma once

#include "stdafx.h"

#include <fstream>

class FileTokenizer
{
public:
	FileTokenizer(string file);
	~FileTokenizer(void);

	bool readAll(vector<vector<string>>& commands);

private:
	vector<string> tokenize(string str);
	string mFile;
};

