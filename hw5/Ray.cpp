#include "Ray.h"

Ray::Ray() {
	mTMin = 0.0001f;
	mTMax = std::numeric_limits<float>::infinity();
	mLastPrimitive = NULL;
	mTime = 0.0f;
}

Ray::Ray(vec3 position, vec3 direction, float time) {
	mPosition = position;
	mDirection = direction;
	mTMin = 0.0001f;
	mTMax = std::numeric_limits<float>::infinity();
	mLastPrimitive = NULL;
	mTime = time;
}



Ray::~Ray(void) {
	mLastPrimitive = NULL;
}

vec3 Ray::getPosition() const {
	return mPosition;
}

void Ray::setPosition(vec3 position) {
	mPosition = position;
}

vec3 Ray::getDirection() const {
	return mDirection;
}

void Ray::setDirection(vec3 direction) {
	mDirection = direction;
}

float Ray::getTMin() const {
	return mTMin;
}

void Ray::setTMin(float value) {
	mTMin = value;
}

float Ray::getTMax() const {
	return mTMax;
}

void Ray::setTMax(float value) {
	mTMax = value;
}

const Primitive* Ray::getLastHitPrimitive() const {
	return mLastPrimitive;
}

void Ray::setLastHitPrimitive(Primitive* primitive) {
	mLastPrimitive = primitive;
}

void Ray::setTime(float time) {
	mTime = time;
}

float Ray::getTime() const {
	return mTime;
}