#include <iostream>

#include "Scene.h"
#include "Utils.h"

#include <stdlib.h>
#include <time.h>

/** Read and render the scene into output file.*/
int main(int argc, char* argv[]) {
	char* inputFile;
	char* outputFile;

	srand ( time(NULL) );

	if (argc > 1) {
		inputFile = argv[1];
	} else {
		inputFile = "data/test13.txt";
	}

	// output file can be overwritten in scene file
	if (argc > 1) {
		outputFile = argv[1];
	} else {
		outputFile = "output1.png";
	}

	Scene(inputFile, outputFile).saveToFile();

	// Uncomment to pause the console
	Utils::pauseConsole();
	return 0;
}

