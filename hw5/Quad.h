#pragma once

#include "stdafx.h"
#include "Shape.h"

class Quad :
	public Shape
{
public:
	Quad(void);
	~Quad(void);

	Quad(vec3 minPosition, vec3 maxPosition);

	virtual bool intersect(const Ray& rRay, float& tHit, LocalGeo& local);
	virtual bool intersectP(const Ray& ray);
	virtual bool intersectP(const AABB& box);

	virtual void findUV(vec3 position, float& x, float& y);

	virtual AABB computeAABB() const;

private:
	vec3 mMinPosition;
	vec3 mMaxPosition;

	Triangle* mTri1;
	Triangle* mTri2;
};

