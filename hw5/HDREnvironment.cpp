#include "HDREnvironment.h"
#include "FreeImage.h"
#include "Ray.h"

HDREnvironment::HDREnvironment(string mapfilename)
{
	if (mapfilename.find(".pfm") != string::npos)
		mHDRMap = FreeImage_Load(FIF_PFM, mapfilename.c_str(), PFM_DEFAULT);
	else
	{
		cout << endl;
		cerr << "Error: Unsupported HDR map format in " << mapfilename << endl;
		exit(1);
	}

	if (!mHDRMap)
	{
		cout << endl;
		cerr << "Error: HDR map \"" << mapfilename << "\" not found/loaded!" << endl;
		exit(1);
	}

	mWidth = FreeImage_GetWidth(mHDRMap);
	mHeight = FreeImage_GetHeight(mHDRMap);
	mPitch = FreeImage_GetPitch(mHDRMap);
	mBPP = FreeImage_GetBPP(mHDRMap);
}

HDREnvironment::~HDREnvironment()
{
	FreeImage_Unload(mHDRMap);
}

vec3 HDREnvironment::radiance(Ray& ray)
{
	// Direction --> Angular map coordinate conversion
	// Courtesy of Paul Debevec and Dan Lemmon's
	// SIGGRAPH 2001 Image-Based Lighting course notes

	vec3 dir = ray.getDirection();

	float r = 0.159154943*acos(dir[2])/sqrt(dir[0]*dir[0] + dir[1]*dir[1]);
	float u = 0.5 + dir[0] * r;
	float v = 0.5 + dir[1] * r;

	// Now get the radiance out of the HDR map
	int col = (int)(u * mWidth);
	int row = (int)((1-v) * mHeight);

	vec3 rad;

	FIRGBF* pixels = (FIRGBF*)FreeImage_GetBits(mHDRMap);

	rad[0] = pixels[row*mHeight + col].red;
	rad[1] = pixels[row*mHeight + col].green;
	rad[2] = pixels[row*mHeight + col].blue;

	return rad;
}