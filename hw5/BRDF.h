#pragma once

#include "stdafx.h"

enum BRDFType
{
	Emissive,
	Diffuse,
	Glossy,
	Mirror,
	Dielectric
};

class BRDF
{
public:
	BRDF();

	~BRDF(void);

	vec3 getKa() const;
	void setKa(vec3 ka);

	vec3 getKd() const;
	void setKd(vec3 kd);

	vec3 getKs() const;
	void setKs(vec3 ks);

	vec3 getKe() const;
	void setKe(vec3 ks);

	float getKr() const;
	void setKr(float kr);

	float getShininess() const;
	void setShininess(float shininess);

	float getKt() const;
	void setKt(float kt);

	void setRefractiveIndex(float index);
	float getRefractiveIndex() const;

	bool isGlossy() const;
	void setIsGlossy(bool isGlossy);

	int getGlossySamplingRate() const;
	void setGlossySamplingRate(int samplingRate);

	float getGlossyDegreeOfBlur() const;
	void setGlossyDegreeOfBlur(float degreeOfBlur);

	BRDFType type() const;
	void setType(BRDFType type);

private:
	// kd, ks, ka are diffuse, specular and ambient component respectively
	// kr is the mirror reflection coefficient
	BRDFType mType;
	vec3 mKa, mKd, mKs, mKe;
	float mShininess;
	float mKt; //Refraction
	float mKr;
	bool mIsGlossy;
	int mGlossySamplingRate;
	float mGlossyDegreeOfBlur;
	float mRefractiveIndex;
};

