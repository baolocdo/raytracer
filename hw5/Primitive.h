#pragma once

#include "stdafx.h"

class Primitive
{
public:
	Primitive(void);
	~Primitive(void);
	virtual bool intersect(const Ray& ray, float& tHit, Intersection& in) = 0;     
	virtual bool intersectP(const Ray& ray) = 0;
	virtual bool intersectP(const AABB& box) = 0;
	virtual BRDF getBRDF(const LocalGeo& local) = 0;
	virtual Material* getMaterial() const = 0;

	virtual AABB getAABB() const = 0;
};

