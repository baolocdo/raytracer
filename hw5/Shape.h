#pragma once

#include "stdafx.h"
#include "AABB.h"

class Shape
{
public:
	Shape(void);
	~Shape(void);

	virtual bool intersect(const Ray& rRay, float& tHit, LocalGeo& local) = 0;
	virtual bool intersectP(const Ray& ray) = 0;
	virtual bool intersectP(const AABB& box) = 0;

	virtual void findUV(vec3 position, float& x, float& y) = 0;

	virtual AABB computeAABB() const = 0;
};

