#pragma once

#include "stdafx.h"

//Store the local geometry at the intersection point
class LocalGeo
{
public:
	LocalGeo();
	LocalGeo(vec3 position, vec3 normal);
	~LocalGeo(void);

	vec3 getPosition() const;
	void setPosition(vec3 position);

	vec3 getNormal() const;
	void setNormal(vec3 normal);

	void getUVCoordinate(float& u, float& v);
	void setUVCoordinate(float u, float v);

private:
	vec3 mPosition;
	vec3 mNormal;
	float mU, mV;
};

