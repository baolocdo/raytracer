#pragma once

#include "Light.h"

class AreaLight
	: public Light
{
public:
	AreaLight(void);
	AreaLight(vec3 position, vec3 edge1, vec3 edge2, vec3 color, vec3 attenuation, int samplingRate);
	~AreaLight(void);

	virtual vec3 shading(LocalGeo& local, Material* material, Ray& lightRay, vec3 eyeDir);
	virtual vec3 lightColor(LocalGeo& local, Material* material, AggregatePrimitive* primitives, Ray& ray);
	void generateLightRay(LocalGeo& local, Ray& lightRay);

	vec3 generateRandomPoint();

protected: 
	vec3 mEdge1, mEdge2;
	vec3 mPosition;
	int mSamplingRate;
	vec3 mAttenuation;
};

