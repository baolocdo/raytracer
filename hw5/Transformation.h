#pragma once

#include "stdafx.h"

class Transformation
{
public:
	Transformation();
	Transformation(mat4 transformMatrix);
	~Transformation(void);

	mat4 getTransform() const;
	void setTransform(const mat4 transform);

	Transformation getInverseTranspose() const;
	Transformation getInverse();

	friend Ray operator * (const Transformation& transformation, const Ray& ray);
	friend LocalGeo operator * (const Transformation& transformation, const LocalGeo& local);
	friend Transformation operator * (const Transformation& transformation, const mat4 vector);
	friend AABB operator * (const Transformation& transformation, const AABB& box);

private:
	mat4 mTransformMatrix;
	//Inverse Transpose for transforming normal
	mat4 mInverseTransposeMatrix; 
};

