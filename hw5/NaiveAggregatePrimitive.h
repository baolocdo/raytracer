#pragma once

#include "stdafx.h"
#include "AggregatePrimitive.h"

class NaiveAggregatePrimitive
	: public AggregatePrimitive
{
public:
	NaiveAggregatePrimitive(vector<Primitive*> primitives);
	~NaiveAggregatePrimitive(void);

	virtual bool intersect(const Ray& ray, float& tHit, Intersection& intersection);
	virtual bool intersectP(const Ray& ray);
};

