#include "Transformation.h"
#include "AABB.h"
#include "Ray.h"
#include "LocalGeo.h"

Transformation::Transformation() {
	setTransform(mat4(1.0));
}

Transformation::Transformation(mat4 transformMatrix) {
	setTransform(transformMatrix);
}

Transformation::~Transformation(void)
{
}

mat4 Transformation::getTransform() const {
	return mTransformMatrix;
}

void Transformation::setTransform(const mat4 transformMatrix) {
	mTransformMatrix = transformMatrix;
	mInverseTransposeMatrix = glm::transpose(glm::inverse(transformMatrix));
}

Transformation Transformation::getInverse() {
	return Transformation(glm::inverse(mTransformMatrix));
}

Transformation Transformation::getInverseTranspose() const {
	return Transformation(mInverseTransposeMatrix);
}

Ray operator*(const Transformation& transformation, const Ray& ray) {
	vec4 newPosition = transformation.mTransformMatrix * vec4(ray.getPosition(), 1.0f);
	vec4 newDirection = transformation.mTransformMatrix * vec4(ray.getDirection(), 0.0f);

	return Ray(vec3(newPosition), vec3(newDirection));
}

LocalGeo operator*(const Transformation& transformation, const LocalGeo& local) {
	vec4 newPosition = transformation.mTransformMatrix * vec4(local.getPosition(), 1.0f);
	vec4 newNormal = transformation.mInverseTransposeMatrix * vec4(local.getNormal(), 0.0f);

	//To be implemented
	return LocalGeo(vec3(newPosition.x, newPosition.y, newPosition.z), 
		glm::normalize(vec3(newNormal.x, newNormal.y, newNormal.z)));
}

Transformation operator*(const Transformation& transformation, const mat4 matrix) {
	return Transformation(transformation.mTransformMatrix * matrix);
}

AABB operator * (const Transformation& transformation, const AABB& box) {
	return AABB(vec3(transformation.mTransformMatrix * vec4(box.getMinVector(), 1.0f)),
		vec3(transformation.mTransformMatrix * vec4(box.getMaxVector(), 1.0f)));
}
