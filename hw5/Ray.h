#pragma once

#include "stdafx.h"

class Ray
{
public:
	Ray();
	//Ray(vec3 position, vec3 direction);
	Ray(vec3 position, vec3 direction, float time = 0.0f);

	~Ray(void);

	vec3 getPosition() const;
	void setPosition(vec3 position);

	vec3 getDirection() const;
	void setDirection(vec3 direction);

	float getTMin() const;
	void setTMin(float value);

	float getTMax() const;
	void setTMax(float value);

	const Primitive* getLastHitPrimitive() const;
	void setLastHitPrimitive(Primitive* primitive);

	void setTime(float time);
	float getTime() const;

private:
	vec3 mPosition;
	vec3 mDirection;
	float mTMin;
	float mTMax;

	float mTime;

	//Last Primitive hit by the ray
	Primitive* mLastPrimitive;

};

