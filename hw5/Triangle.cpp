#include "Triangle.h"
#include "Ray.h"
#include "LocalGeo.h"
#include "Primitive.h"

Triangle::Triangle(vec3 vertice1, vec3 vertice2, vec3 vertice3)
	: mVertice1(vertice1), mVertice2(vertice2), mVertice3(vertice3)
{
	vec3 normal = glm::normalize(glm::cross(mVertice2 - mVertice1, mVertice3 - mVertice1));
	mNormal1 = normal;
	mNormal2 = normal;
	mNormal3 = normal;
}


Triangle::~Triangle(void)
{
}

Triangle::Triangle(vec3 vertice1, vec3 vertice2, vec3 vertice3,	vec3 normal1, vec3 normal2, vec3 normal3) 
	: mVertice1(vertice1), mVertice2(vertice2), mVertice3(vertice3), 
	mNormal1(normal1), mNormal2(normal2), mNormal3(normal3)
{

}

AABB Triangle::computeAABB() const {
	AABB box;
	box.setMaxVector(mVertice1);
	box.setMinVector(mVertice1);
	box.merge(mVertice2);
	box.merge(mVertice3);
	return box;
}

vec3 Triangle::interpolateNormal(float beta, float gamma) {
	vec3 normal = ((1 - beta - gamma) * mNormal1 + beta * mNormal2 + gamma * mNormal3);

	return(glm::normalize(normal));
}

bool Triangle::intersect(const Ray& ray, float& tHit, LocalGeo& local) {
	vec3 normal = glm::normalize(glm::cross(mVertice2 - mVertice1, mVertice3 - mVertice1));

	tHit = - glm::dot((ray.getPosition() - mVertice1), normal) / (glm::dot(ray.getDirection(), normal));
	vec3 planeIntersection = ray.getPosition() + tHit * ray.getDirection();

	float a = glm::dot(glm::cross(mVertice2 - mVertice1, planeIntersection - mVertice1), normal);
	float b = glm::dot(glm::cross(mVertice3 - mVertice2, planeIntersection - mVertice2), normal);
	float c = glm::dot(glm::cross(mVertice1 - mVertice3, planeIntersection - mVertice3), normal); 

	bool isInside = (a >= 0)
		&& (b >= 0)
		&& (c >= 0);

	if (tHit <= ray.getTMin() || tHit > ray.getTMax())
		return false;

	if (isInside) {
		float numerator = ((mVertice2.y - mVertice3.y)*(mVertice1.x - mVertice3.x) + (mVertice3.x - mVertice2.x) * (mVertice1.y - mVertice3.y));

		if (numerator != 0.0f) {
			float lambda1 = ((mVertice2.y - mVertice3.y) * (planeIntersection.x - mVertice3.x) + (mVertice3.x - mVertice2.x)* (planeIntersection.y - mVertice3.y))
				/ numerator;
			float lambda2 = ((mVertice3.y - mVertice1.y) * (planeIntersection.x - mVertice3.x) + (mVertice1.x - mVertice3.x)* (planeIntersection.y - mVertice3.y))
				/ numerator;
			float lambda3 = 1 - lambda1 - lambda2;

			normal = glm::normalize(lambda1 * mNormal1 + lambda2 * mNormal2 + lambda3 * mNormal3);
		}

		//cout << "Triangle Normal: " << normal.x << ", " << normal.y << ", " << normal.z << ". Length: " << glm::length(normal) << endl;
		local.setNormal(normal);
		local.setPosition(planeIntersection);
	}

	return isInside;
}

bool Triangle::intersectP(const Ray& ray) {
	vec3 normal = glm::normalize(glm::cross(mVertice2 - mVertice1, mVertice3 - mVertice1));

	float tHit = - glm::dot((ray.getPosition() - mVertice1), normal) / (glm::dot(ray.getDirection(), normal));
	vec3 planeIntersection = ray.getPosition() + tHit * ray.getDirection();

	if (tHit <= ray.getTMin() || tHit > ray.getTMax())
		return false;

	//Test whether the planeIntersection is inside the triangle or not
	return (glm::dot(glm::cross(mVertice2 - mVertice1, planeIntersection - mVertice1), normal) >= 0)
		&& (glm::dot(glm::cross(mVertice3 - mVertice2, planeIntersection - mVertice2), normal) >= 0)
		&& (glm::dot(glm::cross(mVertice1 - mVertice3, planeIntersection - mVertice3), normal) >= 0);
}

inline bool planeBoxOverlap(vec3 normal, vec3 vert, vec3 maxbox) {
	vec3 vmin, vmax;

	if(normal.x > 0.0f)
	{
		vmin.x = -maxbox.x - vert.x;
		vmax.x = maxbox.x - vert.x;
	}
	else
	{
		vmin.x = maxbox.x - vert.x;
		vmax.x = -maxbox.x - vert.x;
	}

	if(normal.y > 0.0f)
	{
		vmin.y = -maxbox.y - vert.y;
		vmax.y = maxbox.y - vert.y;
	}
	else
	{
		vmin.y = maxbox.y - vert.y;
		vmax.y = -maxbox.y - vert.y;
	}

	if(normal.z > 0.0f)
	{
		vmin.z = -maxbox.z - vert.z;
		vmax.z = maxbox.z - vert.z;
	}
	else
	{
		vmin.z = maxbox.z - vert.z;
		vmax.z = -maxbox.z - vert.z;
	}

	if (glm::dot(normal, vmin) > 0.0f)
		return false;
	if (glm::dot(normal, vmax) >= 0.0f)
		return true;

	return false;
}

#define FINDMINMAX(x0,x1,x2,min,max) \
	min = max = x0;   \
	if(x1<min) min=x1;\
	if(x1>max) max=x1;\
	if(x2<min) min=x2;\
	if(x2>max) max=x2;
/*======================== X-tests ========================*/
#define AXISTEST_X01(a, b, fa, fb)			   \
	p0 = a*v0.y - b*v0.z;			       	   \
	p2 = a*v2.y - b*v2.z;			       	   \
	if(p0<p2) {min=p0; max=p2;} else {min=p2; max=p0;} \
	rad = fa * boxHalfSize.y + fb * boxHalfSize.z;   \
	if(min>rad || max<-rad) return 0;

#define AXISTEST_X2(a, b, fa, fb)			   \
	p0 = a*v0.y - b*v0.z;			           \
	p1 = a*v1.y - b*v1.z;			       	   \
	if(p0<p1) {min=p0; max=p1;} else {min=p1; max=p0;} \
	rad = fa * boxHalfSize.y + fb * boxHalfSize.z;   \
	if(min>rad || max<-rad) return 0;

/*======================== Y-tests ========================*/
#define AXISTEST_Y02(a, b, fa, fb)			   \
	p0 = -a*v0.x + b*v0.z;		      	   \
	p2 = -a*v2.x + b*v2.z;	       	       	   \
	if(p0<p2) {min=p0; max=p2;} else {min=p2; max=p0;} \
	rad = fa * boxHalfSize.x + fb * boxHalfSize.z;   \
	if(min>rad || max<-rad) return 0;

#define AXISTEST_Y1(a, b, fa, fb)			   \
	p0 = -a*v0.x + b*v0.z;		      	   \
	p1 = -a*v1.x + b*v1.z;	     	       	   \
	if(p0<p1) {min=p0; max=p1;} else {min=p1; max=p0;} \
	rad = fa * boxHalfSize.x + fb * boxHalfSize.z;   \
	if(min>rad || max<-rad) return 0;

/*======================== Z-tests ========================*/
#define AXISTEST_Z12(a, b, fa, fb)			   \
	p1 = a*v1.x - b*v1.y;			           \
	p2 = a*v2.x - b*v2.y;			       	   \
	if(p2<p1) {min=p2; max=p1;} else {min=p1; max=p2;} \
	rad = fa * boxHalfSize.x + fb * boxHalfSize.y;   \
	if(min>rad || max<-rad) return 0;

#define AXISTEST_Z0(a, b, fa, fb)			   \
	p0 = a*v0.x - b*v0.y;				   \
	p1 = a*v1.x - b*v1.y;			           \
	if(p0<p1) {min=p0; max=p1;} else {min=p1; max=p0;} \
	rad = fa * boxHalfSize.x + fb * boxHalfSize.y;   \
	if(min>rad || max<-rad) return 0;

bool Triangle::intersectP(const AABB& box) {
	vec3 boxCenter = box.getCenter();
	vec3 boxHalfSize = box.getHalfSize();

	float min, max, p0, p1, p2, rad, fex, fey, fez;
	vec3 normal;

	vec3 v0 = mVertice1 - boxCenter;
	vec3 v1 = mVertice2 - boxCenter;
	vec3 v2 = mVertice3 - boxCenter;

	// compute triangle edges
	vec3 e0 = v1 - v0;
	vec3 e1 = v2 - v1;
	vec3 e2 = v0 - v2;

	fex = fabsf(e0.x);
	fey = fabsf(e0.y);
	fez = fabsf(e0.z);
	AXISTEST_X01(e0.z, e0.y, fez, fey);
	AXISTEST_Y02(e0.z, e0.x, fez, fex);
	AXISTEST_Z12(e0.y, e0.x, fey, fex);

	fex = fabsf(e1.x);
	fey = fabsf(e1.y);
	fez = fabsf(e1.z);
	AXISTEST_X01(e1.z, e1.y, fez, fey);
	AXISTEST_Y02(e1.z, e1.x, fez, fex);
	AXISTEST_Z0(e1.y, e1.x, fey, fex);

	fex = fabsf(e2.x);
	fey = fabsf(e2.y);
	fez = fabsf(e2.z);
	AXISTEST_X2(e2.z, e2.y, fez, fey);
	AXISTEST_Y1(e2.z, e2.x, fez, fex);
	AXISTEST_Z12(e2.y, e2.x, fey, fex);

	/* test in X-direction */
	FINDMINMAX(v0.x, v1.x, v2.x, min, max);
	if(min > boxHalfSize.x || max < -boxHalfSize.x)
		return false;

	/* test in Y-direction */
	FINDMINMAX(v0.y, v1.y, v2.y, min, max);
	if(min > boxHalfSize.y || max < -boxHalfSize.y)
		return false;

	/* test in Z-direction */
	FINDMINMAX(v0.y, v1.z, v2.z, min, max);
	if(min > boxHalfSize.z || max < -boxHalfSize.z)
		return false;

	/*  test if the box intersects the plane of the triangle */
	/*  compute plane equation of triangle: normal*x+d=0 */
	normal = glm::cross(e0, e1);

	return !planeBoxOverlap(normal, v0, boxHalfSize);
}

void Triangle::findUV(vec3 position, float& x, float& y) {

}