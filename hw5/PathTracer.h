#pragma once

#include "Tracer.h"

class PathTracer
	: public Tracer
{
public:
	PathTracer(AggregatePrimitive* primitives, vector<Light*> lights, int maxDepth, Environment* evironment, int numberOfSampleRays = 32);
	~PathTracer(void);

	virtual vec3 trace(Ray& ray, int depth);

private:
	Ray createReflectRay(LocalGeo& local, Ray& ray);

	vec3 dieletricRefract(Ray& ray, Intersection& intersection, int depth, vec3 reflColor);
	bool dielectricCalc(Ray& ray, Intersection& intersection, Ray& refractRay, float& schlick);
	bool createRefractedRay(Ray& ray, Intersection& intersection, float oldIndex, float newIndex, Ray& refractRay);
	float schlickCalc(float index1, float index2, vec3& rayDirection, vec3& refractDirection, vec3& normal);

	Ray& generateRandomRay(LocalGeo& local);

	AggregatePrimitive* mLightPrimitives;
	int mNumberOfSampleRays;
};

