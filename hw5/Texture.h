#pragma once

#include "stdafx.h"
#include "FreeImage.h"

class Texture
{
public:
	Texture(void);
	Texture(string fileName);
	~Texture(void);

	void setTexture(string fileName);

	vec3 getColorAt(float u, float v);
private:
	string mFileName;
	FIBITMAP* mImage;
};

