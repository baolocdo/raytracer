#include "AABB.h"
#include "Ray.h"
#include "Primitive.h"

#define MIN(a,b) (a < b ? a : b)
#define MAX(a,b) (a < b ? b : a)

AABB::AABB(void)
	: mMin(vec3(0.0f)), mMax(vec3(0.0f))
{
}

AABB::AABB(vec3 min, vec3 max)
	: mMin(min), mMax(max)
{
}

AABB::AABB(vector<Primitive*> primitives)
	: mMin(vec3(0.0f)), mMax(vec3(0.0f))
{
	for (int i = 0; i < primitives.size(); i++) {
		merge(primitives[i]->getAABB());
	}
}

AABB::~AABB(void)
{
}

void AABB::merge(AABB& box) {
	mMin.x = MIN(mMin.x, box.mMin.x);
	mMin.y = MIN(mMin.y, box.mMin.y);
	mMin.z = MIN(mMin.z, box.mMin.z);

	mMax.x = MAX(mMax.x, box.mMax.x);
	mMax.y = MAX(mMax.y, box.mMax.y);
	mMax.z = MAX(mMax.z, box.mMax.z);
}

void AABB::merge(vec3 point) {
	mMin.x = MIN(mMin.x, point.x);
	mMin.y = MIN(mMin.y, point.y);
	mMin.z = MIN(mMin.z, point.z);

	mMax.x = MAX(mMax.x, point.x);
	mMax.y = MAX(mMax.y, point.y);
	mMax.z = MAX(mMax.z, point.z);
}

bool AABB::intersectP(Ray ray, float& tmin, float& tmax) {
	vec3 dirfrac(1.0f / ray.getDirection().x, 1.0f / ray.getDirection().y, 1.0f / ray.getDirection().z);
	// ray.getDirection() is unit direction vector of ray

	// mMin is the corner of AABB with minimal coordinates - left bottom, mMax is maximal corner
	// ray.getPosition() is origin of ray
	float t1 = (mMin.x - ray.getPosition().x)*dirfrac.x;
	float t2 = (mMax.x - ray.getPosition().x)*dirfrac.x;
	float t3 = (mMin.y - ray.getPosition().y)*dirfrac.y;
	float t4 = (mMax.y - ray.getPosition().y)*dirfrac.y;
	float t5 = (mMin.z - ray.getPosition().z)*dirfrac.z;
	float t6 = (mMax.z - ray.getPosition().z)*dirfrac.z;

	tmin = max(max(min(t1, t2), min(t3, t4)), min(t5, t6));
	tmax = min(min(max(t1, t2), max(t3, t4)), max(t5, t6));

	// if tmax < 0, ray (line) is intersecting AABB, but whole AABB is behing us
	// if tmin > tmax, ray doesn't intersect AABB
	return !(tmax < 0 || tmin > tmax);
}

void AABB::split(int axis, float position, AABB& minBox, AABB& maxBox) {
	minBox.mMin = mMin;
	minBox.mMax = mMax;
	maxBox.mMin = mMin;
	maxBox.mMax = mMax;

	minBox.mMax[axis] = position;
	maxBox.mMin[axis] = position;
}

bool AABB::contains(vec3& point) const {
	return mMin.x <= point.x && point.x <= mMax.x
		&& mMin.y <= point.y && point.y <= mMax.y
		&& mMin.z <= point.z && point.z <= mMax.z;
}

bool AABB::contains(AABB box) const {
	return mMin.x <= box.mMin.x
		&& mMin.y <= box.mMin.y
		&& mMin.z <= box.mMin.z
		&& box.mMax.x <= mMax.x 
		&& box.mMax.y <= mMax.y 
		&& box.mMax.z <= mMax.z;
}
vec3 AABB::getMinVector() const {
	return mMin;
}

void AABB::setMinVector(vec3 min) {
	mMin = min;
}

void AABB::setMinVectorComponent(int axis, float value) {
	mMin[axis] = value;
}

vec3 AABB::getMaxVector() const {
	return mMax;
}

void AABB::setMaxVector(vec3 max) {
	mMax = max;
}

void AABB::setMaxVectorComponent(int axis, float value) {
	mMax[axis] = value;
}

vec3 AABB::getCenter() const {
	return (mMax + mMin) * 0.5f;
}

vec3 AABB::getSize() const {
	return mMax - mMin;
}

vec3 AABB::getHalfSize() const {
	return (mMax - mMin) * 0.5f;
}

int AABB::getBiggestAxis() const {
	vec3 size = getSize();
	int axis = size[0] > size[1] ? 0 : 1;
	return size[axis] > size[2] ? axis : 2;
}

float AABB::volume() const {
	vec3 diff = mMax - mMin;
	return diff.x * diff.y * diff.z;
}

float AABB::surfaceArea() const {
	vec3 diff = mMax - mMin;
	return diff.x * diff.y * diff.z * 2.0f;
}