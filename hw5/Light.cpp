#include "Light.h"
#include "Ray.h"
#include "Material.h"

Light::Light(void) {
}

Light::~Light(void)
{
}

vec3 Light::getColor() const {
	return mColor;
}

void Light::setColor(vec3 color) {
	mColor = color;
}

vec3 Light::computeLight(LocalGeo& local, Material* material, Ray& lightRay, vec3 eyeDirn) {
	BRDF brdf = material->getBRDF(local);
	vec3 diffuse = material->getColor(local);

	vec3 halfD = glm::normalize(lightRay.getDirection() - eyeDirn) ;  

	float nDotL = glm::dot(local.getNormal(), lightRay.getDirection());
	vec3 lambert = diffuse * mColor * glm::max(nDotL, 0.0f) ;  

	float nDotH = glm::dot(local.getNormal(), halfD); 
	vec3 phong = brdf.getKs() * mColor * pow (glm::max(nDotH, 0.0f), brdf.getShininess()) ; 

	return lambert + phong;
}

