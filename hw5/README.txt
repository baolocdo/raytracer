Name1: Loc Do
Login1: cs184-eb

Name2: Thanh Hai Mai
Login2: cs184-cz

For a better information and images please visit the website: www.locdo.info

In this project, we optimized our ray tracer, perfected some previous features and added more features into our project. Because the time is short (we implemented this project in a few days and there is a final week ahead), we could not make fancier global illumination or photon mapping for caustic effect.
Here are new features:
Global Illumination: we implemented a simple path tracer to trace a number of bouncing rays from diffuse surface. Instead of tracing perfect reflected ray like in the previous project, we perturb the reflected rays (randomly pick rays in the upper hemisphere) and use Monte Carlo to find the color at a particular pixel. The path tracer runs much slower than the ray tracer.
Environment Mapping: we added Environment class, ConstantEnvironment class that returns the constant color background and HDREnvironment class that stores the *.pfm environment file. When a ray misses all the objects, it will return the environment color instead of returning black background like the previous project.
Motion Blur: we added time variable to each ray so that each ray will trace at different time. For a given interval, we find the (startTime + random * endTime) and set that time into a ray. This will make the moving objects look blur. 
New Primitives: We use math methods to find out the intersections of the rays with the primitives (they are not made from triangles so we can save space storage and run faster)
	+ Box
	+ Cone 
	+ Cylinder
We also optimize our Kd tree acceleration. Now a 100,000 triangle Stanford Dragon scene runs in less than 120s

We fixed our Refraction feature to output more realistic by using Beer's law. The code was referenced from the book Fundamentals of Computer Graphics 3rd Edition by Peter Shirley and Steve Marschner

