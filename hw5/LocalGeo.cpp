#include "LocalGeo.h"

LocalGeo::LocalGeo() {

}

LocalGeo::~LocalGeo(void)
{
}

LocalGeo::LocalGeo(vec3 position, vec3 normal) {
	mPosition = position;
	mNormal = normal;
}

vec3 LocalGeo::getPosition() const {
	return mPosition;
}

void LocalGeo::setPosition(vec3 position) {
	mPosition = position;
}

vec3 LocalGeo::getNormal() const {
	return mNormal;
}

void LocalGeo::setNormal(vec3 normal) {
	mNormal = normal;
}

void LocalGeo::getUVCoordinate(float& u, float &v) {
	u = mU;
	v = mV;
}

void LocalGeo::setUVCoordinate(float u, float v) {
	mU = u;
	mV = v;
}