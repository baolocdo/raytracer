#pragma once

#include "stdafx.h"

enum RenderMode {RAYTRACER, PATHTRACER};

class Tracer
{
public:
	Tracer(AggregatePrimitive* primitives, vector<Light*> lights, int maxDepth, Environment* environment);
	~Tracer(void);

	virtual vec3 trace(Ray& ray, int depth) = 0;

protected:
	AggregatePrimitive* mPrimitives;
	Environment* mEnvironment;
	vector<Light*> mLights;

	float mCurrentRefractiveIndex;
	int mMaxDepth;
};

