#include "ConstantEnvironment.h"

ConstantEnvironment::ConstantEnvironment(vec3 color)
	: mBackgroundColor(color)
{
}

vec3 ConstantEnvironment::radiance(Ray& ray)
{
	return mBackgroundColor;
}


